package net.aunacraft.cloud.player;

import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutPlayerExecuteCommand;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

public class ProxyNetworkPlayer extends AbstractNetworkPlayer {

    public ProxyNetworkPlayer(UUID uuid, String name, RunningService currentService) {
        super(uuid, name, currentService);
    }

    @Override
    public void executeCommand(String command) {
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(this.getUUID());
        ProxyServer.getInstance().getPluginManager().dispatchCommand(player, command);
    }

    @Override
    public void sendToService(Service service) {
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(this.getUUID());
        player.connect(ProxyServer.getInstance().getServerInfo(service.getName()));
    }

    @Override
    public void executeBungeeCommand(String command) {
        executeCommand(command);
    }

    @Override
    public void executeSpigotCommand(String command) {
        BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutPlayerExecuteCommand(this.getUUID(), command, true));
    }
}
