package net.aunacraft.cloud;

import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.loader.SimpleBridgeLoader;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroupType;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.listener.ConnectionListener;
import net.aunacraft.cloud.listener.PingListener;
import net.aunacraft.cloud.player.ProxyNetworkPlayer;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;

import java.net.InetSocketAddress;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class CloudPlugin extends Plugin {

    @Getter
    private static CloudPlugin instance;

    @Override
    public void onEnable() {
        instance = this;
        new BridgeClient(new Loader());

        Stream.of(
                new ConnectionListener(),
                new PingListener()
        ).forEach(listener -> getProxy().getPluginManager().registerListener(this, listener));



        getProxy().getScheduler().schedule(this, () -> {
            try {
                BridgeClient.getInstance().sendUpdatePacket();
            } catch (Exception exception) {
            }
        }, 1, 1, TimeUnit.SECONDS);
    }

    private static class Loader extends SimpleBridgeLoader {

        public Loader() {
            super(25565, ServiceGroupType.PROXY);
        }

        @Override
        public int getOnlinePlayerAmount() {
            return BungeeCord.getInstance().getOnlineCount();
        }

        @Override
        public void stopService() {
            BungeeCord.getInstance().stop();
        }

        @Override
        public void addService(Service service) {
            ServerInfo serverInfo =
                    ProxyServer.getInstance()
                            .constructServerInfo(service.getName(),
                                    new InetSocketAddress("localhost", service.getPort()),
                                    "A aunacloud subserver",
                                    false);
            BungeeCord.getInstance().getServers().put(service.getName(), serverInfo);
            BridgeClient.LOGGER.info("Registered service " + service.getName());
        }

        @Override
        public void removeService(Service service) {
            BungeeCord.getInstance().getServers().remove(service.getName());
        }

        @Override
        public AbstractNetworkPlayer createPlayerInstance(UUID uuid, String name, RunningService service) {
            return new ProxyNetworkPlayer(uuid, name, service);
        }
    }
}
