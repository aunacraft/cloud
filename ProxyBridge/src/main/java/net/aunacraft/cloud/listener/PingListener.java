package net.aunacraft.cloud.listener;

import net.aunacraft.cloud.api.AunaCloudAPI;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PingListener implements Listener {

    @EventHandler
    public void handlePing(ProxyPingEvent event) {
        ServerPing ping = event.getResponse();
        ServerPing.Players players = ping.getPlayers();
        players.setMax(AunaCloudAPI.getServiceAPI().getLocalService().getMaxPlayers());
        event.setResponse(ping);
    }
}
