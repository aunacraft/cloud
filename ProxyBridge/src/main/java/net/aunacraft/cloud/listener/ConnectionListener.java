package net.aunacraft.cloud.listener;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutAddPlayer;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutRemovePlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ConnectionListener implements Listener {

    private List<UUID> joining = Lists.newArrayList();
    private HashMap<UUID, String> joinService = Maps.newHashMap();

    @EventHandler
    public void onServerKick(ServerKickEvent event) {
        ProxiedPlayer player = event.getPlayer();
        if(event.getState() == ServerKickEvent.State.CONNECTING) {
            TextComponent component = new TextComponent("§8>> §bAunaCloud §8<<\n§cKicked from server with reason:\n\n");
            for (BaseComponent baseComponent : event.getKickReasonComponent()) {
                baseComponent.setColor(ChatColor.RED);
                component.addExtra(new TextComponent(baseComponent));
            }
            event.setKickReasonComponent(new BaseComponent[]{component});
            player.disconnect(component);
            return;
        }
        if (!player.isConnected()) return;
        RunningService fallbackService = BridgeClient.getInstance().getServiceGroupCache().getBestFallbackService(event.getKickedFrom().getName());
        if(fallbackService != null) {
            if(!fallbackService.getGroup().getGroupName().equals(BridgeClient.getInstance().getServiceGroupCache().getFallbackGroup()))
                event.getPlayer().sendMessage(new TextComponent(BridgeClient.PREFIX + "§cNo lobby service could be found, so you will be sent to §e" + fallbackService.getName() + "§c."));
            ServerInfo serverInfo = ProxyServer.getInstance().getServers().get(fallbackService.getName());
            event.setCancelServer(serverInfo);
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void handlePostLogin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();
        if(!player.hasPermission("cloud.premiumjoin") && AunaCloudAPI.getServiceAPI().getLocalService().isFull()) {
            player.disconnect(new TextComponent("§8>> §bAunaCloud §8<<\n§cThe server is full"));
            return;
        }
        joining.add(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onServerConnect(ServerConnectEvent event) {
        if (!event.getPlayer().getUniqueId().toString().isEmpty()) {
            if (!this.joining.contains(event.getPlayer().getUniqueId())) return;

            ProxiedPlayer player = event.getPlayer();
            this.joining.remove(event.getPlayer().getUniqueId());

            RunningService fallbackService = BridgeClient.getInstance().getServiceGroupCache().getLobbyService("");
            if (fallbackService == null) {
                player.disconnect(new TextComponent("§8>> §bAunaCloud §8<<\n§cCan't find a lobby service"));
                event.setCancelled(true);
            } else {
                ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(fallbackService.getName());
                event.setTarget(serverInfo);
                BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutAddPlayer(player.getUniqueId(), player.getName(), serverInfo.getName()));
                joinService.put(player.getUniqueId(), serverInfo.getName());
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerDisconnectEvent event) {
        joinService.remove(event.getPlayer().getUniqueId());
        if(PlayerCache.exists(event.getPlayer().getUniqueId()))
            BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutRemovePlayer(event.getPlayer().getUniqueId()));
    }
}
