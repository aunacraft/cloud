package net.aunacraft.cloud.api.service;

import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutStartSpigotService;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutStopService;
import net.aunacraft.cloud.bridgeclient.services.ServiceGroupCache;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

import java.util.List;

public class ServiceAPI {

    /**
     * @return Returns the service where the api is running
     */
    public Service getLocalService() {
        return BridgeClient.getInstance().getServiceGroupCache().getLocalService();
    }

    /**
     * @return Returns the current proxy service
     */
    public RunningService getCurrentProxyService() {
        return BridgeClient.getInstance().getServiceGroupCache().getProxyServiceGroup().getOnlineServices().stream().findAny().orElse(null);
    }

    /**
     * @return Returns the service with certain name
     */
    public Service getService(String serviceName) {
        return BridgeClient.getInstance().getServiceGroupCache().getService(serviceName);
    }

    /**
     * @return Returns all services from a group
     */
    @Deprecated
    public List<RunningService> getServices(String groupName) {
        return BridgeClient.getInstance().getServiceGroupCache().getServiceGroup(groupName).getOnlineServices();
    }

    public void startService(String groupName) {
        BridgeClient.getInstance()
                .getNetworkManager()
                .sendPacket(new PacketOutStartSpigotService(groupName));
    }

    public void stopService(Service service) {
        BridgeClient.getInstance()
                .getNetworkManager()
                .sendPacket(new PacketOutStopService(service.getName()));
    }

    public List<ServiceGroup> getGroups() {
        return BridgeClient.getInstance().getServiceGroupCache().getServiceGroups();
    }

    public boolean groupExist(String groupName) {
        return this.getServices(groupName).size() > 0;
    }

    public List<Service> getAllServices() {
        return BridgeClient.getInstance().getServiceGroupCache().getAllServices();
    }

    public ServiceGroup getFallbackGroup() {
        ServiceGroupCache cache = BridgeClient.getInstance().getServiceGroupCache();
        return cache.getServiceGroup(cache.getFallbackGroup());
    }

    public ServiceGroup getGroup(String name) {
        return BridgeClient.getInstance().getServiceGroupCache().getServiceGroup(name);
    }
}
