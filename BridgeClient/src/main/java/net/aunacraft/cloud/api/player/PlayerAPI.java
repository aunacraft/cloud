package net.aunacraft.cloud.api.player;

import com.google.common.collect.Lists;
import net.aunacraft.cloud.bridgeclient.network.player.NetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;

import java.util.List;
import java.util.UUID;

public class PlayerAPI {

    public NetworkPlayer getPlayer(String name) {
        return PlayerCache.getPlayer(name);
    }

    public NetworkPlayer getPlayerByOnlineUUID(UUID uuid) {
        return PlayerCache.getPlayer(uuid);
    }

    public List<NetworkPlayer> getOnlinePlayers() {
        return Lists.newArrayList(PlayerCache.getPlayers());
    }
}
