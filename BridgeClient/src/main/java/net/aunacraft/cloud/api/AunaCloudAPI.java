package net.aunacraft.cloud.api;

import lombok.Getter;
import net.aunacraft.cloud.api.event.EventAPI;
import net.aunacraft.cloud.api.message.MessageAPI;
import net.aunacraft.cloud.api.player.PlayerAPI;
import net.aunacraft.cloud.api.service.ServiceAPI;

public class AunaCloudAPI {

    @Getter
    private final static EventAPI eventAPI = new EventAPI();
    @Getter
    private final static ServiceAPI serviceAPI = new ServiceAPI();
    @Getter
    private final static MessageAPI messageAPI = new MessageAPI();
    @Getter
    private final static PlayerAPI playerAPI = new PlayerAPI();
}
