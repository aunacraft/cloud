package net.aunacraft.cloud.api.event;

import net.aunacraft.cloud.api.event.listener.AunaCloudListenerCallable;
import net.aunacraft.cloud.bridgeclient.BridgeClient;

public class EventAPI {

    public void callEvent(AunaCloudEvent event) {
        try {
        AunaCloudHandlerList.getListeners()
                .forEach(
                        (clazz, callables) -> {
                            if (event.getClass().equals(clazz)) {
                                callables.forEach(callable -> callable.call(event));
                            }
                        });
        }catch (Exception e) {
            BridgeClient.LOGGER.error("Error while calling event", e);
        }
    }

    public <T extends AunaCloudEvent> void registerEventListener(
            Class<T> event, AunaCloudListenerCallable<T> callable) {
        AunaCloudHandlerList.register(event, callable);
    }

    public void unregisterEventListener(
            Class<? extends AunaCloudEvent> event, AunaCloudListenerCallable callable) {
        AunaCloudHandlerList.unregister(event, callable);
    }
}
