package net.aunacraft.cloud.api.event.listener;

import net.aunacraft.cloud.api.event.AunaCloudEvent;

public interface AunaCloudListenerCallable<T extends AunaCloudEvent> {

    void call(final T event);
}
