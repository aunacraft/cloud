package net.aunacraft.cloud.api.event.impl;

import lombok.Getter;
import net.aunacraft.cloud.api.event.AunaCloudEvent;

@Getter
public class ServerStartingEvent extends AunaCloudEvent {

    private final String serviceName;

    public ServerStartingEvent(String serviceName) {
        this.serviceName = serviceName;
    }
}
