package net.aunacraft.cloud.api.event.impl;

import lombok.Getter;
import net.aunacraft.cloud.api.event.AunaCloudEvent;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

@Getter
public class ServerStateUpdateEvent extends AunaCloudEvent {

    private final Service service;
    private final Service.ServiceState oldState, newState;

    public ServerStateUpdateEvent(Service service, Service.ServiceState oldState, Service.ServiceState newState) {
        this.service = service;
        this.oldState = oldState;
        this.newState = newState;
    }
}
