package net.aunacraft.cloud.api.event.impl;

import lombok.Getter;
import net.aunacraft.cloud.api.event.AunaCloudEvent;

@Getter
public class ServerTimeOutEvent extends AunaCloudEvent {

    private final String serviceName;

    public ServerTimeOutEvent(String serviceName) {
        this.serviceName = serviceName;
    }
}
