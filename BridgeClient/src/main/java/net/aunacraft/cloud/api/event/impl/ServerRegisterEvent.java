package net.aunacraft.cloud.api.event.impl;

import lombok.Getter;
import net.aunacraft.cloud.api.event.AunaCloudEvent;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

@Getter
public class ServerRegisterEvent extends AunaCloudEvent {

    private final RunningService service;

    public ServerRegisterEvent(RunningService service) {
        this.service = service;
    }
}
