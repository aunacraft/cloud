package net.aunacraft.cloud.api.event.impl;

import lombok.Getter;
import net.aunacraft.cloud.api.event.AunaCloudEvent;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

@Getter
public class ServicePlayerAmountChangedEvent extends AunaCloudEvent {

    private final Service service;

    public ServicePlayerAmountChangedEvent(Service service) {
        this.service = service;
    }
}
