package net.aunacraft.cloud.api.message;

import com.google.common.collect.Lists;

import java.util.List;

public class PluginMessageHandlerList {

    private static final List<PluginMessageListener> listeners = Lists.newArrayList();

    public static void registerListener(PluginMessageListener pluginMessageListener) {
        listeners.add(pluginMessageListener);
    }

    public static void unregisterListener(PluginMessageListener listener) {
        listeners.remove(listener);
    }

    public static void handlePluginMessage(PluginMessage pluginMessage) {
        try {
            synchronized (listeners) {
                listeners.forEach(listeners -> {
                    listeners.listen(pluginMessage);
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
