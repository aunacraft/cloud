package net.aunacraft.cloud.api.event;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import net.aunacraft.cloud.api.event.listener.AunaCloudListenerCallable;

import java.util.HashMap;
import java.util.List;

public class AunaCloudHandlerList {

    @Getter
    private static HashMap<Class<? extends AunaCloudEvent>, List<AunaCloudListenerCallable>>
            listeners = Maps.newHashMap();

    public static void register(
            Class<? extends AunaCloudEvent> event, AunaCloudListenerCallable callable) {
        List<AunaCloudListenerCallable> callables = listeners.get(event);
        if (callables == null) {
            callables = Lists.newArrayList();
            listeners.put(event, callables);
        }
        callables.add(callable);
    }

    public static void unregister(
            Class<? extends AunaCloudEvent> event, AunaCloudListenerCallable callable) {
        if (listeners.containsKey(event)) {
            listeners.get(event).remove(callable);
        }
    }
}
