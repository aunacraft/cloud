package net.aunacraft.cloud.api.message;

import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutPluginMessage;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

public class MessageAPI {

    public void registerListener(PluginMessageListener listener) {
        PluginMessageHandlerList.registerListener(listener);
    }

    public void unregisterListener(PluginMessageListener listener) {
        PluginMessageHandlerList.unregisterListener(listener);
    }

    public void sendPluginMessage(String toServiceName, PluginMessage pluginMessage) {
        if (pluginMessage == null || toServiceName == null || toServiceName.isEmpty() || pluginMessage.getType() == null || pluginMessage.getType().isEmpty() || pluginMessage.propertiesToString().isEmpty()) {
            throw new NullPointerException("Plugin message is not complete");
        }
        BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutPluginMessage(toServiceName, pluginMessage));
    }

    public void sendPluginMessage(Service service, PluginMessage pluginMessage) {
        this.sendPluginMessage(service.getName(), pluginMessage);
    }

    public void sendPluginMessageToAllServices(PluginMessage pluginMessage) {
        for (ServiceGroup group : AunaCloudAPI.getServiceAPI().getGroups()) {
            group.getOnlineServices().forEach(runningService -> sendPluginMessage(runningService, pluginMessage));
        }
    }

}
