package net.aunacraft.cloud.api.message;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class PluginMessage {

    private static final Type mapType = new TypeToken<HashMap<String, String>>(){}.getType();

    private final String type;
    private final Map<String, String> properties;

    public PluginMessage(String type) {
        this.properties = new HashMap<>();
        this.type = type;
    }

    public PluginMessage(String type, String properties) {
        this.type = type;
        this.properties = new Gson().fromJson(properties, mapType);
    }

    public void set(String key, Object value) {
        this.properties.put(key, value.toString());
    }

    private String getValue(String key) {
        if (!properties.containsKey(key))
            throw new NullPointerException("Plugin-Message does not contain a value with the key '" + key + "'");
        return this.properties.get(key);
    }

    public boolean getBoolean(String key) {
        if (this.isNull(key)) return false;
        return Boolean.parseBoolean(getValue(key));
    }

    public int getInt(String key) {
        return Integer.parseInt(this.getValue(key));
    }

    public long getLong(String key) {
        return Long.parseLong(this.getValue(key));
    }

    public float getFloat(String key) {
        return Float.parseFloat(this.getValue(key));
    }

    public double getDouble(String key) {
        return Double.parseDouble(this.getValue(key));
    }

    public String getString(String key) {
        return this.getValue(key);
    }

    public short getShort(String key) {
        return Short.parseShort(this.getValue(key));
    }

    public byte getByte(String key) {
        return Byte.parseByte(this.getValue(key));
    }

    public boolean isNull(String key) {
        return !this.properties.containsKey(key);
    }

    public String propertiesToString() {
        return new Gson().toJson(this.properties, mapType);
    }

    public String getType() {
        return type;
    }
}
