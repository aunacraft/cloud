package net.aunacraft.cloud.api.message;

public interface PluginMessageListener {

    void listen(PluginMessage pluginMessage);
}
