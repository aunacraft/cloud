package net.aunacraft.cloud.bridgeclient.loader;

import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.NetworkPlayer;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroupType;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

import java.util.UUID;

public interface BridgeLoader {

    int getOnlinePlayerAmount();

    int getServerPort();

    ServiceGroupType getServiceGroupType();

    int getCurrentMemory();

    void stopService();

    void addService(Service service);

    void removeService(Service service);

    AbstractNetworkPlayer createPlayerInstance(UUID uuid, String name, RunningService service);

    void registered(RunningService localService);
}
