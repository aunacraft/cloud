package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

import java.util.UUID;

public class PacketInSendPlayer implements Packet {

    private UUID uuid;
    private String serviceName;

    public PacketInSendPlayer(UUID uuid, String serviceName) {
        this.uuid = uuid;
        this.serviceName = serviceName;
    }

    public PacketInSendPlayer() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.serviceName = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        AbstractNetworkPlayer player = PlayerCache.getPlayer(this.uuid);
        Service service = AunaCloudAPI.getServiceAPI().getService(this.serviceName);
        player.sendToService(service);
    }
}
