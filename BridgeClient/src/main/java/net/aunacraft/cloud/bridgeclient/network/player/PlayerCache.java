package net.aunacraft.cloud.bridgeclient.network.player;

import com.google.common.collect.Lists;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class PlayerCache {

    private static final CopyOnWriteArrayList<AbstractNetworkPlayer> PLAYERS = Lists.newCopyOnWriteArrayList();

    public static void addPlayer(AbstractNetworkPlayer player) {
        PLAYERS.add(player);
    }

    public static void removePlayer(AbstractNetworkPlayer player) {
        PLAYERS.remove(player);
    }

    public static AbstractNetworkPlayer getPlayer(String name) {
        return PLAYERS.stream().filter(player -> player.getName().equals(name)).findAny().orElse(null);
    }

    public static AbstractNetworkPlayer getPlayer(UUID uuid) {
        return PLAYERS.stream().filter(player -> player.getUUID().equals(uuid)).findAny().orElse(null);
    }

    public static boolean exists(UUID uuid) {
        return getPlayer(uuid) != null;
    }

    public static List<NetworkPlayer> getPlayersOnService(RunningService service) {
        if(service.getGroup().equals(BridgeClient.getInstance().getServiceGroupCache().getProxyServiceGroup())) {
            return Lists.newArrayList(PLAYERS);
        }
        List<NetworkPlayer> players = Lists.newArrayList();
        PLAYERS.stream().filter(player -> player.getCurrentService() != null && player.getCurrentService().equals(service)).forEach(players::add);
        return players;
    }

    public static CopyOnWriteArrayList<AbstractNetworkPlayer> getPlayers() {
        return PLAYERS;
    }
}
