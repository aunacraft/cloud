package net.aunacraft.cloud.bridgeclient.network.packets;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;

public interface Packet {

    BridgeClient brideClient = BridgeClient.getInstance();

    void write(ByteBuf byteBuf);

    void read(ByteBuf byteBuf);

    void handle(NetworkManager networkManager);
}
