package net.aunacraft.cloud.bridgeclient.network.util;

import com.google.common.collect.Lists;
import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.NetworkPlayer;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroupType;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

public class ByteBufUtils {

    public static void writeString(ByteBuf byteBuf) {
        writeString(byteBuf, "UTF-8");
    }

    public static void writeString(ByteBuf byteBuf, String string) {
        byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
        byteBuf.writeInt(bytes.length);
        byteBuf.writeBytes(bytes);
    }

    public static String readString(ByteBuf byteBuf) {
       return readString(byteBuf, "UTF-8");
    }
    public static String readString(ByteBuf byteBuf, String charsetName) {
        int len = byteBuf.readInt();
        byte[] bytes = new byte[len];
        byteBuf.readBytes(bytes);
        String string = new String(bytes);
        return string;
    }

    public static void writeService(ByteBuf byteBuf, RunningService service) {
        byteBuf.writeInt(service.getPort());
        byteBuf.writeInt(service.getCurrentMemory());
        byteBuf.writeInt(service.getCurrentState().getId());
        writeString(byteBuf, service.getName());
        writeString(byteBuf, service.getGroup().getGroupName());
        writeString(byteBuf, service.getDisplayName());
        writeString(byteBuf, service.getScreenName());
        writeString(byteBuf, service.getFilePath());
        byteBuf.writeInt(service.getMaxPlayers());
    }

    public static RunningService readService(ByteBuf byteBuf) {
        int port = byteBuf.readInt();
        int currentMemory = byteBuf.readInt();
        Service.ServiceState serviceState = Service.ServiceState.getByID(byteBuf.readInt());
        String name = readString(byteBuf, "UTF-8");
        String groupName = readString(byteBuf, "UTF-8");
        String displayName = readString(byteBuf, "UTF-8");
        String screenName = readString(byteBuf, "UTF-8");
        String filePath = readString(byteBuf, "UTF-8");
        int maxPlayers = byteBuf.readInt();
        ServiceGroup group = BridgeClient.getInstance().getServiceGroupCache().getServiceGroup(groupName);
        if (group == null) throw new NullPointerException("Cant find servicegroup with name: " + groupName);
        RunningService runningService = new RunningService(name, group, displayName, screenName, filePath, serviceState, maxPlayers, port);
        runningService.setCurrentMemory(currentMemory);
        return runningService;
    }

    public static void writeServiceGroupList(ByteBuf byteBuf, List<ServiceGroup> groups) {
        byteBuf.writeInt(groups.size());
        for (ServiceGroup group : groups) {
            writeGroup(byteBuf, group);
        }
    }

    public static List<ServiceGroup> readGroupList(ByteBuf byteBuf) {
        List<ServiceGroup> list = Lists.newArrayList();
        int size = byteBuf.readInt();
        for (int i = 0; i < size; i++) {
            list.add(readGroup(byteBuf));
        }
        return list;
    }

    public static void writeGroup(ByteBuf byteBuf, ServiceGroup group) {
        writeString(byteBuf, group.getGroupName());
        writeString(byteBuf, group.getDisplayName());
        byteBuf.writeInt(group.getMaxPlayers());
        byteBuf.writeInt(group.getMaxMemory());
        byteBuf.writeInt(group.getMaxOnlineServices());
        byteBuf.writeInt(group.getMinOnlineServices());
        byteBuf.writeBoolean(group.isStatic());
        byteBuf.writeInt(group.getType().getId());
        byteBuf.writeInt(group.getPriority());
    }

    public static ServiceGroup readGroup(ByteBuf byteBuf) {
        String groupName = readString(byteBuf);
        String displayName = readString(byteBuf);
        int maxPlayers = byteBuf.readInt();
        int maxMemory = byteBuf.readInt();
        int maxOnlineServices = byteBuf.readInt();
        int minOnlineServices = byteBuf.readInt();
        boolean isStatic = byteBuf.readBoolean();
        ServiceGroupType serviceGroupType = ServiceGroupType.getById(byteBuf.readInt());
        int priority = byteBuf.readInt();
        return new ServiceGroup(groupName, displayName, isStatic, maxOnlineServices, minOnlineServices, maxPlayers, maxMemory, serviceGroupType, priority);
    }

    public static void writePlayer(ByteBuf byteBuf, NetworkPlayer player) {
        writeString(byteBuf, player.getName());
        writeString(byteBuf, player.getUUID().toString());
        writeString(byteBuf, player.getCurrentService().getName());
    }

    public static AbstractNetworkPlayer readPlayer(ByteBuf byteBuf) {
        String name = readString(byteBuf);
        UUID uuid = UUID.fromString(readString(byteBuf));
        String serviceName = readString(byteBuf);
        RunningService service = BridgeClient.getInstance().getServiceGroupCache().getService(serviceName);
        AbstractNetworkPlayer player = BridgeClient.getInstance().getLoader().createPlayerInstance(uuid, name, service);
        return player;
    }

    public static void writePlayerList(ByteBuf byteBuf, List<AbstractNetworkPlayer> players) {
        byteBuf.writeInt(players.size());
        for (AbstractNetworkPlayer player : players) {
            writePlayer(byteBuf, player);
        }
    }

    public static List<AbstractNetworkPlayer> readPlayerList(ByteBuf byteBuf) {
        List<AbstractNetworkPlayer> players = Lists.newArrayList();
        int size = byteBuf.readInt();
        for (int i = 0; i < size; i++) {
            players.add(readPlayer(byteBuf));
        }
        return players;
    }
}
