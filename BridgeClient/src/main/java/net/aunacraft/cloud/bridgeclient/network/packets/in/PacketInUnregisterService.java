package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.event.impl.ServerUnregisterEvent;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

public class PacketInUnregisterService implements Packet {

    private String serviceName;

    public PacketInUnregisterService(String serviceName) {
        this.serviceName = serviceName;
    }

    public PacketInUnregisterService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.serviceName = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(NetworkManager networkManager) {
        RunningService service = BridgeClient.getInstance().getServiceGroupCache().getService(this.serviceName);
        if (service == null) return;
        AunaCloudAPI.getEventAPI().callEvent(new ServerUnregisterEvent(service));
        service.getGroup().removeService(service);
        BridgeClient.getInstance().getLoader().removeService(service);
    }
}
