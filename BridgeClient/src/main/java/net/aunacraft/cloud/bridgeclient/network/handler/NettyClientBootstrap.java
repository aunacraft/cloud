package net.aunacraft.cloud.bridgeclient.network.handler;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NettyClientBootstrap {

    public NettyClientBootstrap() {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(new EpollEventLoopGroup());
        bootstrap.handler(new ClientChannelInitializer());
        bootstrap.channel(EpollSocketChannel.class);
        bootstrap.connect("localhost", 3333);
    }
}
