package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.event.impl.ServerStartingEvent;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

public class PacketInStartingSpigotService implements Packet {

    private String serviceName;

    public PacketInStartingSpigotService(String serviceName) {
        this.serviceName = serviceName;
    }

    public PacketInStartingSpigotService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.serviceName = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(NetworkManager networkManager) {
        AunaCloudAPI.getEventAPI().callEvent(new ServerStartingEvent(this.serviceName));
    }
}
