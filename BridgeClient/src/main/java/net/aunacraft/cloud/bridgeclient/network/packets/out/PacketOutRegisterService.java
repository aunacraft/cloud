package net.aunacraft.cloud.bridgeclient.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;

public class PacketOutRegisterService implements Packet {

    private int port;

    public PacketOutRegisterService(int port) {
        this.port = port;
    }

    public PacketOutRegisterService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        byteBuf.writeInt(this.port);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.port = byteBuf.readInt();
    }

    @Override
    public void handle(NetworkManager networkManager) {
    }
}
