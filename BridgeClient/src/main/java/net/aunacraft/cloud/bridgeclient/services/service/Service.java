package net.aunacraft.cloud.bridgeclient.services.service;

import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.network.player.NetworkPlayer;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;

import java.util.List;

public interface Service {

    String getName();

    ServiceGroup getGroup();

    int getMaxPlayers();

    String getDisplayName();

    String getScreenName();

    int getCurrentMemory();

    List<NetworkPlayer> getOnlinePlayers();

    ServiceState getCurrentState();

    int getPort();

    void setMaxPlayers(int maxPlayers);

    void sendUpdate();

    default boolean isFull() {
        return this.getOnlinePlayers().size() >= this.getMaxPlayers();
    }

    @Getter
    enum ServiceState {

        STARTING(0), RUNNING(1), STOPPING(2), WAIT_FOR_STOP(3);

        private final int id;

        ServiceState(int id) {
            this.id = id;
        }

        public static ServiceState getByID(int id) {
            for (ServiceState value : values()) {
                if(value.getId() == id) return value;
            }
            return null;
        }
    }
}
