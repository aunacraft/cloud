package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

@Getter
public class PacketInUpdateService implements Packet {

    private RunningService updatedService;

    public PacketInUpdateService(RunningService service) {
        this.updatedService = service;
    }

    public PacketInUpdateService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeService(byteBuf, updatedService);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.updatedService = ByteBufUtils.readService(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        RunningService service = BridgeClient.getInstance().getServiceGroupCache().getService(this.updatedService.getName());
        if(service == null) return;
        service.handleUpdatePacket(this);
    }
}
