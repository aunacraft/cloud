package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

import java.util.List;

public class PacketInRegisterPlayers implements Packet {

    private List<AbstractNetworkPlayer> players;

    public PacketInRegisterPlayers(List<AbstractNetworkPlayer> players) {
        this.players = players;
    }

    public PacketInRegisterPlayers() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writePlayerList(byteBuf, players);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.players = ByteBufUtils.readPlayerList(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        players.forEach(PlayerCache::addPlayer);
    }
}
