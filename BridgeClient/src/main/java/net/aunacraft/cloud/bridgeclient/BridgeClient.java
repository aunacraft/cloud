package net.aunacraft.cloud.bridgeclient;

import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.cloud.bridgeclient.loader.BridgeLoader;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.handler.NettyClientBootstrap;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutUpdateService;
import net.aunacraft.cloud.bridgeclient.services.ServiceGroupCache;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Getter
public class BridgeClient {

    public static final Logger LOGGER = LoggerFactory.getLogger("BridgeClient");
    @Getter
    private static BridgeClient instance;
    private final BridgeLoader loader;
    public static final String PREFIX = "§bAunaCloud §8| §7";

    private final ServiceGroupCache serviceGroupCache = new ServiceGroupCache();

    private final NetworkManager networkManager = new NetworkManager(this);
    private boolean registered = false;

    @SneakyThrows
    public BridgeClient(BridgeLoader loader) {
        BridgeClient.instance = this;
        this.loader = loader;
        LOGGER.info("Connecting to Cloud-Manager...");
        new NettyClientBootstrap();
    }

    public void finishRegistration(
            RunningService localService,
            RunningService proxyService,
            String fallbackGroupName) {
        serviceGroupCache.setProxyServiceGroup(proxyService.getGroup());
        serviceGroupCache.setLocalService(localService);
        serviceGroupCache.setFallbackGroup(fallbackGroupName);
        serviceGroupCache.getLocalService().setCurrentState(Service.ServiceState.RUNNING);
        this.loader.registered(localService);
        LOGGER.info("Cloud-Manager registered service as " + localService.getName());
        this.registered = true;

    }

    public void sendUpdatePacket() {
        if(serviceGroupCache.getLocalService() == null) return;
        Runtime runtime = Runtime.getRuntime();
        long memoryInBytes = runtime.totalMemory() - runtime.freeMemory();
        int memoryInMB = (int) (memoryInBytes / (1024L * 1024L));
        RunningService localService = serviceGroupCache.getLocalService();
        localService.setCurrentMemory(memoryInMB);
        localService.sendUpdate();
    }

}
