package net.aunacraft.cloud.bridgeclient.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

public class PacketOutUpdateService implements Packet {

    private RunningService service;

    public PacketOutUpdateService(RunningService service) {
        this.service = service;
    }

    public PacketOutUpdateService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeService(byteBuf, service);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.service = ByteBufUtils.readService(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {

    }
}
