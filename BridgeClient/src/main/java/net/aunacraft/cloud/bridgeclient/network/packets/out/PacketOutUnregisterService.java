package net.aunacraft.cloud.bridgeclient.network.packets.out;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

@Getter
public class PacketOutUnregisterService implements Packet {

    private String serviceName;

    public PacketOutUnregisterService(String serviceName) {
        this.serviceName = serviceName;
    }

    public PacketOutUnregisterService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.serviceName = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(NetworkManager networkManager) {

    }
}
