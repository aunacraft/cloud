package net.aunacraft.cloud.bridgeclient.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

import java.util.UUID;

public class PacketOutPlayerSwitchService implements Packet {

    private UUID uuid;
    private String serviceName;

    public PacketOutPlayerSwitchService(UUID uuid, String serviceName) {
        this.uuid = uuid;
        this.serviceName = serviceName;
    }

    public PacketOutPlayerSwitchService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, this.serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.serviceName = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        RunningService service = BridgeClient.getInstance().getServiceGroupCache().getService(this.serviceName);
        AbstractNetworkPlayer player = PlayerCache.getPlayer(uuid);
        player.setCurrentService(service);
    }
}
