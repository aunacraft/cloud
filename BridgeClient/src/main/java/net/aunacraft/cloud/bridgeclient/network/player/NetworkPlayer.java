package net.aunacraft.cloud.bridgeclient.network.player;

import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

import java.util.UUID;

public interface NetworkPlayer {

    UUID getUUID();
    String getName();
    RunningService getCurrentService();

    void sendToService(Service service);
    void executeBungeeCommand(String command);
    void executeSpigotCommand(String command);
}
