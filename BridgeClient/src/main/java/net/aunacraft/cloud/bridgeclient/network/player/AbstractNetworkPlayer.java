package net.aunacraft.cloud.bridgeclient.network.player;

import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

import java.util.UUID;

public abstract class AbstractNetworkPlayer implements NetworkPlayer {

    private final UUID uuid;
    private final String name;
    private RunningService currentService;

    public AbstractNetworkPlayer(UUID uuid, String name, RunningService currentService) {
        this.uuid = uuid;
        this.name = name;
        this.currentService = currentService;
    }

    @Override
    public UUID getUUID() {
        return this.uuid;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public RunningService getCurrentService() {
        return currentService;
    }

    public void setCurrentService(RunningService currentService) {
        this.currentService = currentService;
    }

    public abstract void executeCommand(String command);
}
