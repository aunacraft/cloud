package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutKeepAlive;

public class PacketInKeepAlive implements Packet {

    private int id;

    public PacketInKeepAlive(int id) {
        this.id = id;
    }

    public PacketInKeepAlive() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        byteBuf.writeInt(this.id);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.id = byteBuf.readInt();
    }

    @Override
    public void handle(NetworkManager networkManager) {
        networkManager.sendPacket(new PacketOutKeepAlive(id));
    }
}
