package net.aunacraft.cloud.bridgeclient.network;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutRegisterService;

import java.io.IOException;

public class NetworkManager extends SimpleChannelInboundHandler<Packet> {

    private final BridgeClient bridgeClient;
    @Getter
    private Channel channel;

    public NetworkManager(BridgeClient bridgeClient) {
        this.bridgeClient = bridgeClient;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.channel = ctx.channel();
        super.channelActive(ctx);
        this.sendPacket(new PacketOutRegisterService(bridgeClient.getLoader().getServerPort()));
    }

    public void sendPacket(Packet packet) {
        this.channel.writeAndFlush(packet);
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        BridgeClient.LOGGER.warn("Disconnected from cloud");
        BridgeClient.getInstance().getLoader().stopService();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet packet) throws Exception {
        packet.handle(this);
    }
}
