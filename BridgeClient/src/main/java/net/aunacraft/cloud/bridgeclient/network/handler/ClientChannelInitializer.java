package net.aunacraft.cloud.bridgeclient.network.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.PacketRegistry;

public class ClientChannelInitializer extends ChannelInitializer<Channel> {

    @Override
    protected void initChannel(Channel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();

        pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4));
        pipeline.addLast(new PacketDecoder(PacketRegistry.registry));

        pipeline.addLast(new LengthFieldPrepender(4));
        pipeline.addLast(new PacketEncoder(PacketRegistry.registry));

        pipeline.addLast(BridgeClient.getInstance().getNetworkManager());


    }
}
