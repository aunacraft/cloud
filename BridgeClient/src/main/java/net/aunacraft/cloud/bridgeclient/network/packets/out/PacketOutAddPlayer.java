package net.aunacraft.cloud.bridgeclient.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

import java.util.UUID;

public class PacketOutAddPlayer implements Packet {

    private UUID uuid;
    private String name;
    private String service;

    public PacketOutAddPlayer(UUID uuid, String name, String service) {
        this.uuid = uuid;
        this.name = name;
        this.service = service;
    }

    public PacketOutAddPlayer() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, name);
        ByteBufUtils.writeString(byteBuf, service);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.name = ByteBufUtils.readString(byteBuf);
        this.service = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {

    }
}
