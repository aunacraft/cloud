package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.event.impl.ServerRegisterEvent;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

public class PacketInAddSpigotService implements Packet {

    private RunningService service;

    public PacketInAddSpigotService(RunningService service) {
        this.service = service;
    }

    public PacketInAddSpigotService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeService(byteBuf, service);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.service = ByteBufUtils.readService(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        service.getGroup().addService(service);
        AunaCloudAPI.getEventAPI().callEvent(new ServerRegisterEvent(this.service));
        BridgeClient.getInstance().getLoader().addService(service);
    }
}
