package net.aunacraft.cloud.bridgeclient.network.handler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.packets.PacketRegistry;

import java.util.List;

public class PacketDecoder extends ByteToMessageDecoder {

    private final PacketRegistry packetRegistry;

    public PacketDecoder(PacketRegistry packetRegistry) {
        this.packetRegistry = packetRegistry;
    }

    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        if(!byteBuf.isReadable()) return;

        int i = byteBuf.readInt();
        if (i > 0) {
            try {
                int id = byteBuf.readInt();
                if(!packetRegistry.containsPacket(PacketRegistry.PacketDirection.IN, id)) {
                    BridgeClient.LOGGER.error("Cant find packet for id " + id);
                    return;
                }
                Packet packet = packetRegistry.createPacket(id);
                packet.read(byteBuf);
                list.add(packet);
            } catch (Exception e) {
                e.printStackTrace();
                BridgeClient.LOGGER.error("Can't decode packet", e);
            }
        }
    }
}
