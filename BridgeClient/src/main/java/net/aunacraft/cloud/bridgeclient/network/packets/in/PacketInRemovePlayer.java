package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

import java.util.UUID;

public class PacketInRemovePlayer implements Packet {

    private UUID uuid;

    public PacketInRemovePlayer(UUID uuid) {
        this.uuid = uuid;
    }

    public PacketInRemovePlayer() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
    }

    @Override
    public void handle(NetworkManager networkManager) {
        AbstractNetworkPlayer player = PlayerCache.getPlayer(uuid);
        PlayerCache.removePlayer(player);
    }
}
