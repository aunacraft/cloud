package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

public class PacketInStopService implements Packet {

    @Override
    public void write(ByteBuf byteBuf) {
    }

    @Override
    public void read(ByteBuf byteBuf) {
    }

    @Override
    public void handle(NetworkManager networkManager) {
        RunningService localService = BridgeClient.getInstance().getServiceGroupCache().getLocalService();
        if(localService != null) {
            localService.setCurrentState(Service.ServiceState.STOPPING);
        }
        BridgeClient.getInstance().getLoader().stopService();
    }
}
