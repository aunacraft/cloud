package net.aunacraft.cloud.bridgeclient.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.api.message.PluginMessage;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

public class PacketOutPluginMessage implements Packet {

    private String serviceName;
    private PluginMessage pluginMessage;

    public PacketOutPluginMessage(String serviceName, PluginMessage pluginMessage) {
        this.serviceName = serviceName;
        this.pluginMessage = pluginMessage;
    }

    public PacketOutPluginMessage() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        String properties = this.pluginMessage.propertiesToString();
        ByteBufUtils.writeString(byteBuf, this.serviceName);
        ByteBufUtils.writeString(byteBuf, this.pluginMessage.getType());
        ByteBufUtils.writeString(byteBuf, properties);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.serviceName = ByteBufUtils.readString(byteBuf, "UTF-8");
        String type = ByteBufUtils.readString(byteBuf, "UTF-8");
        String properties = ByteBufUtils.readString(byteBuf, "UTF-8");
        this.pluginMessage = new PluginMessage(type, properties);
    }

    @Override
    public void handle(NetworkManager networkManager) {

    }
}
