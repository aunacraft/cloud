package net.aunacraft.cloud.bridgeclient.services;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
public class ServiceGroupCache {

    private final CopyOnWriteArrayList<ServiceGroup> spigotServiceGroups;
    @Setter private ServiceGroup proxyServiceGroup;
    @Setter private String fallbackGroup;
    @Setter private RunningService localService;

    public ServiceGroupCache() {
        this.spigotServiceGroups = Lists.newCopyOnWriteArrayList();
    }

    public void addSpigotGroup(ServiceGroup serviceGroup) {
        this.spigotServiceGroups.add(serviceGroup);
    }

    public List<ServiceGroup> getServiceGroups() {
        List<ServiceGroup> groups = Lists.newArrayList();
        groups.addAll(this.spigotServiceGroups);
        groups.add(this.proxyServiceGroup);
        return groups;
    }

    public ServiceGroup getServiceGroup(String name) {
        return this.getServiceGroups().stream().filter(group -> group.getGroupName().equals(name)).findAny().orElse(null);
    }

    public RunningService getService(String name) {
        for (ServiceGroup serviceGroup : getServiceGroups()) {
            RunningService service = serviceGroup.getOnlineServices().stream().filter(s -> s.getName().equals(name)).findAny().orElse(null);
            if(service == null) continue;
            return service;

        }
        return null;
    }

    public List<Service> getAllServices() {
        List<Service> services = Lists.newArrayList();
        for (ServiceGroup spigotServiceGroup : this.spigotServiceGroups) {
            services.addAll(spigotServiceGroup.getOnlineServices());
        }
        return services;
    }

    public RunningService getBestFallbackService(String kickedServer) {
        RunningService lobbyService = getLobbyService(kickedServer);
        if(lobbyService == null) {
            for (ServiceGroup spigotServiceGroup : this.spigotServiceGroups) {
                for (RunningService onlineService : spigotServiceGroup.getOnlineServices()) {
                    if(!Objects.equals(onlineService.getName(), kickedServer) && onlineService.getOnlinePlayers().size() < onlineService.getMaxPlayers())
                        return onlineService;
                }
            }
        }else {
            return lobbyService;
        }
        return null;
    }

    public RunningService getLobbyService(String kickedServer) {
        RunningService service = null;
        ServiceGroup group = this.getServiceGroup(this.fallbackGroup);
        assert group != null;

        for (RunningService onlineService : group.getOnlineServices()) {
            if(onlineService.getName().equals(kickedServer) || onlineService.getCurrentState() != Service.ServiceState.RUNNING)
                continue;
            int playersOnline = onlineService.getOnlinePlayers().size();

            if (service == null && playersOnline < onlineService.getMaxPlayers())
                service = onlineService;
            else if (service != null && playersOnline < service.getOnlinePlayers().size() && playersOnline < onlineService.getMaxPlayers())
                service = onlineService;

        }
        return service;
    }

    public void setLocalService(RunningService localService) {
        this.localService = localService;
        localService.getGroup().addService(localService);
    }
}
