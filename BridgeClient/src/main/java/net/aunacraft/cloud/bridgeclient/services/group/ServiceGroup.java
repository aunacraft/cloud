package net.aunacraft.cloud.bridgeclient.services.group;

import com.google.common.collect.Lists;
import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import java.util.List;

@Getter
public class ServiceGroup {

    private final String groupName;
    private final String displayName;
    private final boolean isStatic;
    private final int maxOnlineServices;
    private final int minOnlineServices;
    private int maxPlayers;
    private final int maxMemory;
    private final List<RunningService> onlineServices = Lists.newArrayList();
    private final ServiceGroupType type;
    private final int priority;

    public ServiceGroup(String groupName, String displayName, boolean isStatic, int maxOnlineServices, int minOnlineServices, int maxPlayers, int maxMemory, ServiceGroupType type, int priority) {
        this.groupName = groupName;
        this.priority = priority;
        this.displayName = displayName;
        this.isStatic = isStatic;
        this.maxOnlineServices = maxOnlineServices;
        this.minOnlineServices = minOnlineServices;
        this.maxPlayers = maxPlayers;
        this.maxMemory = maxMemory;
        this.type = type;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        this.onlineServices.forEach(service -> service.setMaxPlayers(maxPlayers));
    }

    public void startNewService() {
        //send start packet
    }

    private boolean isUsedServiceName(String name) {
        for (Service onlineService : this.onlineServices) {
            if (onlineService.getName().equals(name)) return true;
        }
        return false;
    }

    public void addService(RunningService service) {
        this.onlineServices.add(service);
    }

    public void removeService(RunningService service) {
        this.onlineServices.remove(service);
    }

    public RunningService getService(String name) {
        return this.onlineServices.stream().filter(runningService -> runningService.getName().equals(name)).findAny().orElse(null);
    }

}
