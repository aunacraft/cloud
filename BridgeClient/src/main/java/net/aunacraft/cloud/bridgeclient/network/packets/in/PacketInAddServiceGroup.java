package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;

import java.util.List;

public class PacketInAddServiceGroup implements Packet {

    private List<ServiceGroup> groups;

    public PacketInAddServiceGroup() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeServiceGroupList(byteBuf, groups);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.groups = ByteBufUtils.readGroupList(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        for (ServiceGroup group : groups) {
            BridgeClient.getInstance().getServiceGroupCache().addSpigotGroup(group);
        }

    }
}
