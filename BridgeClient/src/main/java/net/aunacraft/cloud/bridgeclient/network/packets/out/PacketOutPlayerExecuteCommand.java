package net.aunacraft.cloud.bridgeclient.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

import java.util.UUID;

public class PacketOutPlayerExecuteCommand implements Packet {

    private UUID uuid;
    private String command;
    private boolean proxy;

    public PacketOutPlayerExecuteCommand(UUID uuid, String command, boolean proxy) {
        this.uuid = uuid;
        this.command = command;
        this.proxy = proxy;
    }

    public PacketOutPlayerExecuteCommand() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, command);
        byteBuf.writeBoolean(this.proxy);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.command = ByteBufUtils.readString(byteBuf);
        this.proxy = byteBuf.readBoolean();
    }

    @Override
    public void handle(NetworkManager networkManager) {

    }
}
