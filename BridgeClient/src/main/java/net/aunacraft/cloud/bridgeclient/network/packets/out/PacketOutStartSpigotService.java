package net.aunacraft.cloud.bridgeclient.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

public class PacketOutStartSpigotService implements Packet {

    private String groupName;

    public PacketOutStartSpigotService(String groupName) {
        this.groupName = groupName;
    }

    public PacketOutStartSpigotService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.groupName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.groupName = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(NetworkManager networkManager) {
    }
}
