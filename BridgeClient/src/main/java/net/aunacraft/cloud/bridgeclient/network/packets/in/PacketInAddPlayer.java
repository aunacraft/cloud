package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

import java.util.UUID;

public class PacketInAddPlayer implements Packet {

    private UUID uuid;
    private String name;
    private String service;

    public PacketInAddPlayer(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public PacketInAddPlayer() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, name);
        ByteBufUtils.writeString(byteBuf, service);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.name = ByteBufUtils.readString(byteBuf);
        this.service = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        RunningService service = BridgeClient.getInstance().getServiceGroupCache().getService(this.service);
        AbstractNetworkPlayer player = BridgeClient.getInstance().getLoader().createPlayerInstance(this.uuid, this.name, service);
        PlayerCache.addPlayer(player);
    }
}
