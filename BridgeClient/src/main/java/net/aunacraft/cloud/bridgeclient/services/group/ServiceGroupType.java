package net.aunacraft.cloud.bridgeclient.services.group;

import lombok.Getter;

@Getter
public enum ServiceGroupType {

    SPIGOT(
            "default_spigot.jar",
            "spigot.jar",
            "https://api.pl3x.net/v2/purpur/1.17.1/1393/download",
            0),
    PROXY(
            "default_bungeecord.jar",
            "bungeecord.jar",
            "https://papermc.io/api/v2/projects/waterfall/versions/1.17/builds/449/downloads/waterfall-1.17-449.jar",
            1);

    private final String defaultJarName;
    private final String downloadURL;
    private final String finalJarName;
    private final int id;

    ServiceGroupType(String defaultJarName, String finalJarName, String downloadURL, int id) {
        this.defaultJarName = defaultJarName;
        this.downloadURL = downloadURL;
        this.finalJarName = finalJarName;
        this.id = id;
    }

    public static ServiceGroupType getById(int id) {
        for (ServiceGroupType value : values()) {
            if(value.getId() == id) return value;
        }
        return null;
    }
}
