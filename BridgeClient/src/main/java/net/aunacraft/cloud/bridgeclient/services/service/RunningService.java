package net.aunacraft.cloud.bridgeclient.services.service;

import com.google.common.collect.Lists;
import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.in.PacketInUpdateService;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutUpdateService;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.NetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;

import java.util.List;

@Getter
public class RunningService implements Service {
    private final String name;
    private final ServiceGroup group;
    private final String displayName;
    private int maxPlayers;
    private final String screenName;
    private int currentMemory;
    private final String filePath;
    private final int port;
    private ServiceState currentState;

    public RunningService(String name, ServiceGroup group, String displayName, String screenName, String filePath, ServiceState currentState, int maxPlayers, int port) {
        this.name = name;
        this.filePath = filePath;
        this.group = group;
        this.displayName = displayName;
        this.screenName = screenName;
        this.currentState = currentState;
        this.currentMemory = 0;
        this.port = port;
        this.maxPlayers = maxPlayers;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public ServiceGroup getGroup() {
        return group;
    }

    @Override
    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    @Override
    public String getScreenName() {
        return this.screenName;
    }

    @Override
    public int getCurrentMemory() {
        return this.currentMemory;
    }

    @Override
    public List<NetworkPlayer> getOnlinePlayers() {
        return PlayerCache.getPlayersOnService(this);
    }

    @Override
    public ServiceState getCurrentState() {
        return this.currentState;
    }

    @Override
    public int getPort() {
        return this.port;
    }

    @Override
    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public void setCurrentState(ServiceState currentState) {
        this.currentState = currentState;
    }

    public void setCurrentMemory(int currentMemory) {
        this.currentMemory = currentMemory;
    }

    public void handleUpdatePacket(PacketInUpdateService packet) {
        RunningService updatedService = packet.getUpdatedService();
        this.currentMemory = updatedService.getCurrentMemory();
        this.currentState = updatedService.getCurrentState();
        this.maxPlayers = updatedService.getMaxPlayers();
        this.currentState = updatedService.getCurrentState();
    }

    @Override
    public void sendUpdate() {
        BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutUpdateService(this));
    }

}
