package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.api.message.PluginMessage;
import net.aunacraft.cloud.api.message.PluginMessageHandlerList;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

public class PacketInPluginMessage implements Packet {

    private PluginMessage pluginMessage;

    public PacketInPluginMessage(PluginMessage pluginMessage) {
        this.pluginMessage = pluginMessage;
    }

    public PacketInPluginMessage() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.pluginMessage.getType());
        ByteBufUtils.writeString(byteBuf, this.pluginMessage.propertiesToString());
    }

    @Override
    public void read(ByteBuf byteBuf) {
        String type = ByteBufUtils.readString(byteBuf, "UTF-8");
        String properties = ByteBufUtils.readString(byteBuf, "UTF-8");
        this.pluginMessage = new PluginMessage(type, properties);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        PluginMessageHandlerList.handlePluginMessage(this.pluginMessage);
    }
}
