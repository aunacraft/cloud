package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.PlayerCache;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;

import java.util.UUID;

public class PacketInPlayerExecuteCommand implements Packet {

    private UUID uuid;
    private String command;

    public PacketInPlayerExecuteCommand(UUID uuid, String command, boolean proxy) {
        this.uuid = uuid;
        this.command = command;
    }

    public PacketInPlayerExecuteCommand() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, command);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.command = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(NetworkManager networkManager) {
        AbstractNetworkPlayer player = PlayerCache.getPlayer(this.uuid);
        player.executeCommand(this.command);
    }
}
