package net.aunacraft.cloud.bridgeclient.loader;

import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroupType;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

public abstract class SimpleBridgeLoader implements BridgeLoader {

    private final int serverPort;
    private final ServiceGroupType serviceGroupType;

    public SimpleBridgeLoader(int serverPort, ServiceGroupType serviceGroupType) {
        this.serverPort = serverPort;
        this.serviceGroupType = serviceGroupType;
    }

    @Override
    public int getServerPort() {
        return serverPort;
    }

    @Override
    public ServiceGroupType getServiceGroupType() {
        return serviceGroupType;
    }

    @Override
    public int getCurrentMemory() {
        return (int) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
    }

    @Override
    public void registered(RunningService localService) {

    }
}
