package net.aunacraft.cloud.bridgeclient.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.NetworkManager;
import net.aunacraft.cloud.bridgeclient.network.packets.Packet;
import net.aunacraft.cloud.bridgeclient.network.util.ByteBufUtils;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroup;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;

import java.util.List;

public class PacketInRegisterService implements Packet {

    private RunningService localService;
    private RunningService proxyService;
    private String fallbackGroupName;
    private long ping;

    public PacketInRegisterService(List<ServiceGroup> spigotServiecGroups, RunningService localService, RunningService proxyService, String fallbackGroupName) {
        this.localService = localService;
        this.proxyService = proxyService;
        this.fallbackGroupName = fallbackGroupName;
    }

    public PacketInRegisterService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeService(byteBuf, this.localService);
        ByteBufUtils.writeService(byteBuf, this.proxyService);
        ByteBufUtils.writeString(byteBuf, this.fallbackGroupName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.localService = ByteBufUtils.readService(byteBuf);
        this.proxyService = ByteBufUtils.readService(byteBuf);
        this.fallbackGroupName = ByteBufUtils.readString(byteBuf, "UTF-8");
        this.ping = byteBuf.readLong();
    }

    @Override
    public void handle(NetworkManager networkManager) {
        BridgeClient.LOGGER.info("Register packet with ping: " + (System.currentTimeMillis() - ping));
        BridgeClient.getInstance()
                .finishRegistration(localService, proxyService, this.fallbackGroupName);
    }
}
