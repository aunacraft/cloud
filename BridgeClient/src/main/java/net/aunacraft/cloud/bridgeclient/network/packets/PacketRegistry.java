package net.aunacraft.cloud.bridgeclient.network.packets;

import com.google.common.collect.Maps;
import net.aunacraft.cloud.bridgeclient.network.packets.in.*;
import net.aunacraft.cloud.bridgeclient.network.packets.out.*;

import java.util.HashMap;
import java.util.Map;

public class PacketRegistry {

    public static PacketRegistry registry = new PacketRegistry();

    private final HashMap<Integer, Class<? extends Packet>> IN_PACKETS = Maps.newHashMap(),
            OUT_PACKETS = Maps.newHashMap();

    public PacketRegistry() {
        addPacket(PacketDirection.OUT, 0, PacketOutRegisterService.class);
        addPacket(PacketDirection.IN, 0, PacketInRegisterService.class);

        addPacket(PacketDirection.OUT, 1, PacketOutKeepAlive.class);
        addPacket(PacketDirection.IN, 1, PacketInKeepAlive.class);

        addPacket(PacketDirection.OUT, 2, PacketOutUpdateService.class);
        addPacket(PacketDirection.IN, 2, PacketInUpdateService.class);

        addPacket(PacketDirection.OUT, 3, PacketOutStartSpigotService.class);
        addPacket(PacketDirection.IN, 3, PacketInAddSpigotService.class);

        addPacket(PacketDirection.OUT, 4, PacketOutStopService.class);
        addPacket(PacketDirection.IN, 4, PacketInStopService.class);

        addPacket(PacketDirection.IN, 5, PacketInUnregisterService.class);
        addPacket(PacketDirection.OUT, 5, PacketOutUnregisterService.class);

        addPacket(PacketDirection.IN, 6, PacketInStartingSpigotService.class);

        addPacket(PacketDirection.OUT, 7, PacketOutPluginMessage.class);
        addPacket(PacketDirection.IN, 7, PacketInPluginMessage.class);

        addPacket(PacketDirection.IN, 8, PacketInAddServiceGroup.class);

        addPacket(PacketDirection.IN, 9, PacketInAddPlayer.class);
        addPacket(PacketDirection.OUT, 9, PacketOutAddPlayer.class);

        addPacket(PacketDirection.IN, 10, PacketInRemovePlayer.class);
        addPacket(PacketDirection.OUT, 10, PacketOutRemovePlayer.class);

        addPacket(PacketDirection.IN, 11, PacketInPlayerSwitchService.class);
        addPacket(PacketDirection.OUT, 11, PacketOutPlayerSwitchService.class);

        addPacket(PacketDirection.IN, 12, PacketInPlayerExecuteCommand.class);
        addPacket(PacketDirection.OUT, 12, PacketOutPlayerExecuteCommand.class);

        addPacket(PacketDirection.IN, 13, PacketInSendPlayer.class);
        addPacket(PacketDirection.OUT, 13, PacketOutSendPlayer.class);

        addPacket(PacketDirection.IN, 14, PacketInRegisterPlayers.class);
    }

    public void addPacket(PacketDirection direction, int id, Class<? extends Packet> packetClass) {
        if (direction == PacketDirection.IN) {
            this.IN_PACKETS.put(id, packetClass);
        } else if (direction == PacketDirection.OUT) {
            this.OUT_PACKETS.put(id, packetClass);
        }
    }

    public boolean containsPacket(PacketDirection direction, int id) {
        return direction.equals(PacketDirection.IN)
                ? IN_PACKETS.containsKey(id)
                : OUT_PACKETS.containsKey(id);
    }

    public Packet createPacket(int id) throws IllegalAccessException, InstantiationException {
        return IN_PACKETS.get(id).newInstance();
    }

    public int getPacketID(Packet packet) {
        for (Map.Entry<Integer, Class<? extends Packet>> packetEntry : OUT_PACKETS.entrySet()) {
            if (packetEntry.getValue().equals(packet.getClass())) {
                return packetEntry.getKey();
            }
        }
        throw new NullPointerException("Can't find id for packet " + packet.getClass().getSimpleName());
    }

    public enum PacketDirection {
        OUT,
        IN;
    }
}
