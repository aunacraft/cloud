package net.aunacraft.cloud.gui.impl;

import net.aunacraft.cloud.CloudPlugin;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.gui.Gui;
import net.aunacraft.cloud.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitTask;

import java.util.Arrays;

public class OnlineServicesGui extends Gui {

    private BukkitTask bukkitTask;

    public OnlineServicesGui(Player opener) {
        super(opener, Bukkit.createInventory(null, 54, "§fOnline-Services"));
        this.init();
    }

    @Override
    protected void init() {
        this.inventory.setItem(27 + 18, new ItemBuilder(Material.ARROW, 1).setDisplayname("§cBack").build());

        for(int i = 28+18; i < 35+18; i++)
            this.inventory.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());

        bukkitTask = Bukkit.getScheduler().runTaskTimer(CloudPlugin.getInstance(), () -> {
            int index = 0;
            for (Service all : BridgeClient.getInstance().getServiceGroupCache().getAllServices()) {
                this.inventory.setItem(index, new ItemBuilder((
                        all.equals(AunaCloudAPI.getServiceAPI().getCurrentProxyService()) ?
                                Material.PAPER : (!all.getCurrentState().equals(Service.ServiceState.RUNNING) ? Material.BUCKET : Material.LAVA_BUCKET)
                ), 1).setDisplayname(all.getName())
                        .setLore(Arrays.asList(
                                "§6Group: §e" + all.getGroup().getGroupName(),
                                "§6Memory: §e" + (String.valueOf(all.getCurrentMemory()).toString().length() > 3 ? String.valueOf(all.getCurrentMemory()).toString().substring(0, 3) : String.valueOf(all.getCurrentMemory()).toString()) + "MB",
                                "§6OnlinePlayers: §e" + all.getOnlinePlayers().size()
                        )).build());
                index++;
            }
        }, 0, 10);
    }

    @Override
    public void handleClose(InventoryCloseEvent event) {
        bukkitTask.cancel();
        Bukkit.getScheduler().cancelTask(bukkitTask.getTaskId());
        super.handleClose(event);
    }
}
