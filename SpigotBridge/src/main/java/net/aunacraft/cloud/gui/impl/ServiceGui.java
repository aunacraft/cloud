package net.aunacraft.cloud.gui.impl;

import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.gui.Gui;
import net.aunacraft.cloud.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class ServiceGui extends Gui {

    private Service service;

    public ServiceGui(Player opener, Service service) {
        super(opener, Bukkit.createInventory(null, 36, "§cService informations about service " + service.getName() + ":"));
        this.service = service;
        this.fill();
        this.init();
    }

    @Override
    protected void init() {
        this.inventory.setItem(11, new ItemBuilder(Material.PAPER, 1).setDisplayname("§b" + this.service.getName()).setLore(
                Arrays.asList(
                        "§6Group: §e" + this.service.getGroup().getGroupName(),
                        "§6Memory: §e" + String.valueOf(this.service.getCurrentMemory() / 1000) + "MB",
                        "§6OnlinePlayers: §e" + this.service.getOnlinePlayers().size(),
                        "§6MaxPlayers: §e" + this.service.getMaxPlayers(),
                        "§6Port: §e" + this.service.getPort(),
                        "§6ServiceState: §e" + this.service.getCurrentState()
                )
        ).build());
        this.inventory.setItem(15, new ItemBuilder(Material.RED_DYE, 1).setDisplayname("§cStop the Service (Rightclick)").build());
        this.inventory.setItem(27, new ItemBuilder(Material.ARROW, 1).setDisplayname("§cBack").build());
        this.inventory.setItem(28, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(29, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(30, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(31, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(32, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(33, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(34, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(35, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
    }
}
