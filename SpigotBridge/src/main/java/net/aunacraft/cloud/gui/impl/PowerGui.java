package net.aunacraft.cloud.gui.impl;

import net.aunacraft.cloud.CloudPlugin;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.gui.Gui;
import net.aunacraft.cloud.utils.ItemBuilder;
import net.aunacraft.cloud.utils.MathUtil;
import net.aunacraft.cloud.watcher.Watcher;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitTask;

import java.util.Arrays;

public class PowerGui extends Gui {

    private BukkitTask bukkitTask;
    private final Service service;
    private final Watcher watcher;

    public PowerGui(Player opener, Service service, Watcher watcher) {
        super(opener, Bukkit.createInventory(null, 9*6, "§eService Memory Diagram (SMD)"));
        this.service = service;
        this.watcher = watcher;
        init();
    }

    @Override
    protected void init() {

        this.inventory.setItem(9*6-9, new ItemBuilder(Material.ARROW, 1).setDisplayname("§cBack").build());
        this.inventory.setItem(9*6-8, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(9*6-7, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(9*6-6, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(9*6-5, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(9*6-4, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(9*6-3, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(9*6-2, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(9*6-1, new ItemBuilder(Material.BOOK, 1).setDisplayname("§bView Service Page for §7" + service.getName()).setLore(
                Arrays.asList(
                        "§eGeneral informations for Service '" + service.getName() + "':",
                        "§6Group: §e" + this.service.getGroup().getGroupName(),
                        "§6Memory: §e" + String.valueOf(this.service.getCurrentMemory() / 1000) + "MB",
                        "§6OnlinePlayers: §e" + this.service.getOnlinePlayers().size(),
                        "§6MaxPlayers: §e" + this.service.getMaxPlayers(),
                        "§6Port: §e" + this.service.getPort(),
                        "§6ServiceState: §e" + this.service.getCurrentState()
                )
        ).build());

        bukkitTask = Bukkit.getScheduler().runTaskTimer(CloudPlugin.getInstance(), () -> {
            int index = 0;
            for (int memory : watcher.array){
                if(memory <= 0){
                    this.inventory.setItem(index, new ItemBuilder(Material.WHITE_STAINED_GLASS_PANE, 1).setDisplayname("Not calculated!").build());
                    this.inventory.setItem(index + 9, new ItemBuilder(Material.WHITE_STAINED_GLASS_PANE, 1).setDisplayname("Not calculated!").build());
                    this.inventory.setItem(index + 9 * 2, new ItemBuilder(Material.WHITE_STAINED_GLASS_PANE, 1).setDisplayname("Not calculated!").build());
                    this.inventory.setItem(index + 9 * 3, new ItemBuilder(Material.WHITE_STAINED_GLASS_PANE, 1).setDisplayname("Not calculated!").build());
                    this.inventory.setItem(index + 9 * 4, new ItemBuilder(Material.WHITE_STAINED_GLASS_PANE, 1).setDisplayname("Not calculated!").build());
                }else{
                    int calculated = MathUtil.percent(memory, service.getGroup().getMaxMemory());

                    this.inventory.setItem(index, new ItemBuilder(Material.AIR, 1).build());
                    this.inventory.setItem(index + 9, new ItemBuilder(Material.AIR, 1).build());
                    this.inventory.setItem(index + 9 * 2, new ItemBuilder(Material.AIR, 1).build());
                    this.inventory.setItem(index + 9 * 3, new ItemBuilder(Material.AIR, 1).build());
                    this.inventory.setItem(index + 9 * 4, new ItemBuilder(Material.AIR, 1).build());
                    if(calculated > 85)
                        this.inventory.setItem(index, new ItemBuilder(Material.RED_STAINED_GLASS_PANE, 1).setDisplayname("§cMemory: " + memory + "mb" + " §b" + calculated + "%").build());
                    if(calculated > 75)
                        this.inventory.setItem(index + 9, new ItemBuilder(Material.YELLOW_STAINED_GLASS_PANE, 1).setDisplayname("§cMemory: " + memory + "mb"+ " §b" + calculated + "%").build());
                    if(calculated > 60)
                        this.inventory.setItem(index + 9 * 2, new ItemBuilder(Material.GREEN_STAINED_GLASS_PANE, 1).setDisplayname("§cMemory: " + memory + "mb"+ " §b" + calculated + "%").build());
                    if(calculated > 35)
                        this.inventory.setItem(index + 9 * 3, new ItemBuilder(Material.GREEN_STAINED_GLASS_PANE, 1).setDisplayname("§cMemory: " + memory + "mb"+ " §b" + calculated + "%").build());
                    if(calculated > 0)
                        this.inventory.setItem(index + 9 * 4, new ItemBuilder(Material.GREEN_STAINED_GLASS_PANE, 1).setDisplayname("§cMemory: " + memory + "mb"+ " §b" + calculated + "%").build());
                }
                index++;
            }
        }, 0, 20);
    }

    @Override
    public void handleClose(InventoryCloseEvent event) {
        if(event.getView().getTitle().equals("§eService Memory Diagram (SMD)")){
            bukkitTask.cancel();
            Bukkit.getScheduler().cancelTask(bukkitTask.getTaskId());
            watcher.close();
        }
        super.handleClose(event);
    }
}
