package net.aunacraft.cloud.gui.impl;

import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroupType;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.gui.Gui;
import net.aunacraft.cloud.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashMap;

public class StartServicesGui extends Gui {
    public StartServicesGui(Player opener) {
        super(opener, Bukkit.createInventory(null, 54, "§aStart-Services"));
        this.init();
    }

    @Override
    protected void init() {
        this.inventory.setItem(27 + 18, new ItemBuilder(Material.ARROW, 1).setDisplayname("§cBack").build());
        this.inventory.setItem(28 + 18, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(29 + 18, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(30 + 18, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(31 + 18, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(32 + 18, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(33 + 18, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(34 + 18, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        this.inventory.setItem(35 + 18, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1).setDisplayname(" ").build());
        HashMap<String, ServiceGroupType> serviceGroups = new HashMap<>();
        for (Service service : BridgeClient.getInstance().getServiceGroupCache().getAllServices()) {
            if (!serviceGroups.containsKey(service.getGroup().getGroupName()))
                serviceGroups.put(service.getGroup().getGroupName(), service.getGroup().getType());
        }
        for (String allGroups : serviceGroups.keySet()) {
            this.inventory.addItem(new ItemBuilder((
                    serviceGroups.get(allGroups).equals(ServiceGroupType.PROXY) ?
                            Material.PAPER : Material.LAVA_BUCKET
            ), 1).setDisplayname(allGroups).setLore(Arrays.asList(serviceGroups.get(allGroups).toString())).build());
        }
    }
}
