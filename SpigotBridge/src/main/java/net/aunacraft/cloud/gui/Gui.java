package net.aunacraft.cloud.gui;

import net.aunacraft.cloud.CloudPlugin;
import net.aunacraft.cloud.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;

public abstract class Gui implements Listener {

    public Player opener;
    public Inventory inventory;

    public Inventory getInventory() {
        return inventory;
    }

    public Player getOpener() {
        return opener;
    }

    public Gui(Player opener, Inventory inventory) {
        this.opener = opener;
        this.inventory = inventory;
        Bukkit.getPluginManager().registerEvents(this, CloudPlugin.getInstance());
    }

    protected abstract void init();

    public void open() {
        this.opener.openInventory(this.inventory);
    }

    public void fill() {
        for (int i = 0; i < 27; i++) {
            this.inventory.setItem(i, new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, 1, (short) 0).setDisplayname(" ").build());
        }
    }

    @EventHandler
    public void handleClose(InventoryCloseEvent event){

    }
}
