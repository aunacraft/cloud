package net.aunacraft.cloud.gui.impl;

import net.aunacraft.cloud.gui.Gui;
import net.aunacraft.cloud.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class IndexCloudGui extends Gui {

    public IndexCloudGui(Player player) {
        super(player, Bukkit.createInventory(null, 27, "§bCloud-GUI"));
        this.fill();
        this.init();
    }

    @Override
    protected void init() {
        this.inventory.setItem(15, new ItemBuilder(Material.ENDER_CHEST, 1).setDisplayname("§eOnline-Services").build());
        this.inventory.setItem(11, new ItemBuilder(Material.HOPPER, 1).setDisplayname("§aStart-Services").build());
    }
}
