package net.aunacraft.cloud.utils;

public class MathUtil {

    public static int percent(double part, double ofAll){
        double one = ofAll / 100;
        double percent = part / one;
        return (int) (percent > 100 ? 100 : percent);
    }

}
