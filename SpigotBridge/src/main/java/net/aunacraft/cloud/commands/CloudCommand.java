package net.aunacraft.cloud.commands;

import net.aunacraft.cloud.CloudPlugin;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.gui.impl.IndexCloudGui;
import net.aunacraft.cloud.utils.HastebinUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class CloudCommand implements CommandExecutor, TabCompleter {

    public CloudCommand(PluginCommand command) {
        command.setExecutor(this);
        command.setTabCompleter(this);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (!commandSender.hasPermission("cloud.admin")) {
            commandSender.sendMessage(BridgeClient.PREFIX + "§bAunaCloud §7by §9FebanHD §7in cooperation with ytendx");
            return false;
        }

        if (args.length == 0)
            sendHelp(commandSender);

        else if (args.length == 1) {
            switch (args[0].toLowerCase(Locale.ROOT)){

                case "list" -> {
                    commandSender.sendMessage(BridgeClient.PREFIX + "§3Here is a List of all online services: ");
                    for (Service all : BridgeClient.getInstance().getServiceGroupCache().getAllServices()) {
                        commandSender.sendMessage(BridgeClient.PREFIX + " §8- §3" + all.getName() + " §7| §3" + all.getGroup().getGroupName());
                    }
                }

                case "gui" -> {
                    if (!(commandSender instanceof Player)) {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cYou have to be a Player to execute this commmand.");
                        return true;
                    }
                    new IndexCloudGui((((Player) commandSender).getPlayer())).open();
                }

                case "consolemonitor", "monitor" -> {
                    if(!(commandSender instanceof Player)){
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cYou have to be a Player to execute this commmand.");
                        return true;
                    }
                    Player player = ((Player) commandSender).getPlayer();

                    if(CloudPlugin.getInstance().getMonitorProvider().getCurrentlyInSession().contains(player)){
                        CloudPlugin.getInstance().getMonitorProvider().remove(player);
                        player.sendMessage(BridgeClient.PREFIX + "§7Your console monitor session was succesfully §cclosed§7.");
                    }else{
                        CloudPlugin.getInstance().getMonitorProvider().add(player);
                        player.sendMessage(BridgeClient.PREFIX + "§7Your console monitor session was succesfully §aopened§7.");
                    }
                }

                case "uploadlog", "hastebinupload", "hastebin", "logupload" -> {

                    File file = new File("logs/latest.log");

                    if(!file.exists()){
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cThe required log file does not exist. Too early command execution?");
                        return false;
                    }

                    try {
                        String content = new String(Files.readAllBytes(file.toPath()));
                        commandSender.sendMessage(BridgeClient.PREFIX + "§7Uploading the log file to hastebin...");
                        String link = HastebinUtil.post(content, false);
                        commandSender.sendMessage(BridgeClient.PREFIX + "§7The file was succesfully uploaded and is now reachable at that link §f" + link);
                    } catch (IOException e) {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cThe file reading process occured an error! §8(§7MSG: §4" + e.getMessage() + "§8)");
                    }
                }
                default -> sendHelp(commandSender);
            }
        } else if (args.length == 2) {

            switch (args[0].toLowerCase(Locale.ROOT)){

                case "start" -> {
                    if (!AunaCloudAPI.getServiceAPI().groupExist(args[1])) {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cA group with that name does not exist. Please refer to the autocoompletions.");
                    } else {
                        commandSender.sendMessage(BridgeClient.PREFIX + "Service is starting...");
                        AunaCloudAPI.getServiceAPI().startService(args[1]);
                        Bukkit.getLogger().info("User " + commandSender.getName() + " started new Service of Group " + args[1]);
                    }
                }

                case "stop", "stopservice", "stops", "shutdownservice", "shutdowns" -> {
                    if (AunaCloudAPI.getServiceAPI().getService(args[1]) != null) {
                        commandSender.sendMessage(BridgeClient.PREFIX + "The Service is stopping...");
                        AunaCloudAPI.getServiceAPI().stopService(Objects.requireNonNull(AunaCloudAPI.getServiceAPI().getService(args[1])));
                    } else {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cThis Service does not exists.");
                    }
                }

                case "stopgroup", "stopg", "shutdowngroup", "shutdowng" -> {
                    if (AunaCloudAPI.getServiceAPI().groupExist(args[1])) {
                        commandSender.sendMessage(BridgeClient.PREFIX + "Stopping services...");
                        for (Service onlineServices : Objects.requireNonNull(AunaCloudAPI.getServiceAPI().getServices(args[1]))) {
                            AunaCloudAPI.getServiceAPI().stopService(onlineServices);
                            commandSender.sendMessage(BridgeClient.PREFIX + "Service " + onlineServices.getName() + " stopped!");
                        }
                        commandSender.sendMessage(BridgeClient.PREFIX + "Stopped all Services!");
                    } else {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cThis group does not exists");
                    }
                }

                case "info" -> {
                    Service target = AunaCloudAPI.getServiceAPI().getService(args[1]);
                    if (target != null) {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§eInformations about service " + target.getName() + ":");
                        commandSender.sendMessage(BridgeClient.PREFIX + "Name: §6" + target.getName());
                        commandSender.sendMessage(BridgeClient.PREFIX + "Memory: §6" + target.getCurrentMemory());
                        commandSender.sendMessage(BridgeClient.PREFIX + "MaxPlayers: §6" + target.getMaxPlayers());
                        commandSender.sendMessage(BridgeClient.PREFIX + "GroupName: §6" + target.getGroup().getGroupName());
                        commandSender.sendMessage(BridgeClient.PREFIX + "DisplayName: §6" + target.getDisplayName());
                        commandSender.sendMessage(BridgeClient.PREFIX + "OnlinePlayers: §6" + target.getOnlinePlayers().size());
                        commandSender.sendMessage(BridgeClient.PREFIX + "Port: §6" + target.getPort());
                        commandSender.sendMessage(BridgeClient.PREFIX + "ServiceState: §6" + target.getCurrentState().toString());
                    } else {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cThis Service doesnt exists!");
                    }
                }

                default -> sendHelp(commandSender);
            }

        } else if (args.length == 3) {
            switch (args[0].toLowerCase(Locale.ROOT)){

                case "setmaxplayers" -> {
                    Service target = AunaCloudAPI.getServiceAPI().getService(args[1]);
                    if(target == null) {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§cThis Service doesnt exists!");
                        return false;
                    }

                    int maxPlayers;
                    try {
                        maxPlayers = Integer.parseInt(args[2]);
                        if(maxPlayers < 0)
                            throw new IllegalArgumentException("Number < 0");
                    }catch (Exception e) {
                        commandSender.sendMessage(BridgeClient.PREFIX + "§4" + args[2] + " §cis not a valid number!");
                        return false;
                    }

                    target.setMaxPlayers(maxPlayers);
                    target.sendUpdate();
                    commandSender.sendMessage("§7The player limit of §b" + target.getName() + " was set to §3" + target.getMaxPlayers() + " §7!");

                }

            }
        }else
            sendHelp(commandSender);
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("cloud.admin")) return null;
        List<String> matches = new ArrayList<>();
        if (args.length == 0 || args.length == 1) {
            matches.addAll(Arrays.asList("gui", "start", "stop", "stopgroup", "list", "info", "consolemonitor", "uploadlog", "setmaxplayers"));
        }
        if (args.length == 2) {
            switch (args[0].toLowerCase()) {
                case "start", "stopgrop" -> addServiceGroupsToList(matches);
                case "stop", "stopservice", "info", "setmaxplayers" -> addServicesToList(matches);
            }
        }

        String start = args.length >= 1 ? args[args.length - 1].toLowerCase() : "";
        matches.removeIf(s -> !s.toLowerCase().startsWith(start));
        Collections.sort(matches);
        return matches;
    }

    private void addServiceGroupsToList(List<String> list) {
        AunaCloudAPI.getServiceAPI().getGroups().forEach(serviceGroup -> list.add(serviceGroup.getGroupName()));
    }

    private void addServicesToList(List<String> list) {
        BridgeClient.getInstance().getServiceGroupCache().getAllServices().forEach(service -> list.add(service.getName()));
    }

    public void sendHelp(CommandSender commandSender){
        commandSender.sendMessage(BridgeClient.PREFIX + "§eAuna-Cloud by FebanHD in cooperation with ytendx");
        commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud gui §7- Opens a Cloud Admin GUI");
        commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud start <ServiceGroup> §7- Starts a new Service from Service Group");
        commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud stopservice <Service> §7- Stops specific Service");
        commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud stopgroup <ServiceGroup> §7- Stops all Services from specific Group");
        commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud list §7- Shows you all online services");
        commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud info <Service> §7- Shows you infos about one service");
        commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud consolemonitor §7- Shows you Console Input.");
        commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud uploadlog §7- Uploads the latest log of the current service to hastebin");
    }

    /*
    *       commandSender.sendMessage(BridgeClient.PREFIX + "§eAuna-Cloud by FebanHD in cooperation with ytendx");
            commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud gui §7- Opens a Cloud Admin GUI");
            commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud start <ServiceGroup> §7- Starts a new Service from Service Group");
            commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud stopservice <Service> §7- Stops specific Service");
            commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud stopgroup <ServiceGroup> §7- Stops all Services from specific Group");
            commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud list §7- Shows you all online services");
            commandSender.sendMessage(BridgeClient.PREFIX + "§3/cloud info <Service> §7- Shows you infos about one service");*/


}
