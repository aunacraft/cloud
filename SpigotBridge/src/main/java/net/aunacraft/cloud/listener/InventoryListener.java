package net.aunacraft.cloud.listener;

import net.aunacraft.cloud.CloudPlugin;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.gui.impl.OnlineServicesGui;
import net.aunacraft.cloud.gui.impl.PowerGui;
import net.aunacraft.cloud.gui.impl.ServiceGui;
import net.aunacraft.cloud.gui.impl.StartServicesGui;
import net.aunacraft.cloud.watcher.Watcher;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getClickedInventory() == null) return;
        if (event.getCurrentItem() == null) return;
        if(!event.getWhoClicked().hasPermission("cloud.admin")) return;
        if (event.getView().getTitle().equalsIgnoreCase("§bCloud-GUI")) {
            if (event.getCurrentItem().hasItemMeta()) {
                event.setCancelled(true);
                if (event.getSlot() == 15) {
                    new OnlineServicesGui((Player) event.getWhoClicked()).open();
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aStart-Services")) {
                    new StartServicesGui((Player) event.getWhoClicked()).open();
                } else {
                    event.setCancelled(true);
                }
            }
        } else if (event.getView().getTitle().contains("Service informations")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cBack")) {
                Bukkit.dispatchCommand(event.getWhoClicked(), "cloud gui");
            }
        } else if (event.getView().getTitle().equalsIgnoreCase("§fOnline-Services")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cBack")) {
                Bukkit.dispatchCommand(event.getWhoClicked(), "cloud gui");
            } else if (event.getCurrentItem().getType().equals(Material.PAPER) || event.getCurrentItem().getType().equals(Material.LAVA_BUCKET)) {
                Service current = BridgeClient.getInstance().getServiceGroupCache().getService(event.getCurrentItem().getItemMeta().getDisplayName());
                if (current == null) {
                    event.getWhoClicked().sendMessage(BridgeClient.PREFIX + "§cThere was a mistake at InventoryListener(java:43). Null Service.");
                } else {
                    new ServiceGui((Player) event.getWhoClicked(), current).open();
                }
            } else {
                event.setCancelled(true);
            }
        } else if (event.getView().getTitle().contains("§cService informations about service ")) {
            String serviceName = event.getView().getTitle().replaceFirst("§cService informations about service ", "") + "";
            serviceName = serviceName.replace(":", "");
            Service current = BridgeClient.getInstance().getServiceGroupCache().getService(serviceName);
            event.setCancelled(true);
            if (event.getCurrentItem().getItemMeta().getDisplayName().contains("Stop the Service")) {
                if (current != null) {
                    if (current instanceof RunningService) {
                        event.getWhoClicked().sendMessage(BridgeClient.PREFIX + "§aService is stopping...");
                        //TODO: stop service
                    }
                }
            } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cBack")) {
                Bukkit.dispatchCommand(event.getWhoClicked(), "cloud gui");
            } else {
                new PowerGui((Player) event.getWhoClicked(), current, CloudPlugin.getInstance().getWatcherProvider().getOrCreate(current)).open();
            }

        } else if (event.getView().getTitle().equalsIgnoreCase("§aStart-Services")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType().equals(Material.PAPER) || event.getCurrentItem().getType().equals(Material.LAVA_BUCKET)) {
                String serviceGroup = event.getCurrentItem().getItemMeta().getDisplayName();
                if (AunaCloudAPI.getServiceAPI().groupExist(serviceGroup))
                    AunaCloudAPI.getServiceAPI().startService(serviceGroup);
                else
                    event.getWhoClicked().sendMessage(BridgeClient.PREFIX + "§cThis ServiceGroup does not exists. Please report that to the System Administrator.");
            } else if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cBack")) {
                Bukkit.dispatchCommand(event.getWhoClicked(), "cloud gui");
            } else {
                event.setCancelled(true);
            }
        }else if(event.getView().getTitle().equals("§eService Memory Diagram (SMD)")){
            event.setCancelled(true);
            if (event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cBack")) {
                new OnlineServicesGui((Player) event.getWhoClicked()).open();
            }else if(event.getCurrentItem().getItemMeta().getDisplayName().contains("§bView Service Page for §7")){
                String service = event.getCurrentItem().getItemMeta().getDisplayName().split("§7")[1];
                Service clooudService = BridgeClient.getInstance().getServiceGroupCache().getService(service);
                if(clooudService != null){
                    new ServiceGui((Player) event.getWhoClicked(), clooudService).open();
                }else{
                    event.getWhoClicked().sendMessage(BridgeClient.PREFIX + "An error occured while trying to view service management page!");
                }
            }
        }
    }

}
