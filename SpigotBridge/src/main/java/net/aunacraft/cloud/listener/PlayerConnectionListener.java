package net.aunacraft.cloud.listener;

import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutPlayerSwitchService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerConnectionListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Service service = AunaCloudAPI.getServiceAPI().getLocalService();
        BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutPlayerSwitchService(event.getPlayer().getUniqueId(), service.getName()));
    }
}
