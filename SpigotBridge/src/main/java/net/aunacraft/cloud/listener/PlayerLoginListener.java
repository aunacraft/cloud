package net.aunacraft.cloud.listener;

import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PlayerLoginListener implements Listener {


    @EventHandler
    public void handleLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();
        if(player.hasPermission("cloud.premiumjoin") || !AunaCloudAPI.getServiceAPI().getLocalService().isFull()) return;
        event.setKickMessage(BridgeClient.PREFIX + "§cThis service is full.");
        event.setResult(PlayerLoginEvent.Result.KICK_FULL);
    }

}
