package net.aunacraft.cloud.listener;

import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.event.impl.ServerRegisterEvent;
import net.aunacraft.cloud.api.event.impl.ServerStartingEvent;
import net.aunacraft.cloud.api.event.impl.ServerTimeOutEvent;
import net.aunacraft.cloud.api.event.impl.ServerUnregisterEvent;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MessageListener {

    public MessageListener() {
        AunaCloudAPI.getEventAPI().registerEventListener(ServerRegisterEvent.class, event -> {
            for (Player all : Bukkit.getOnlinePlayers()) {
                if (all.hasPermission("auna.cloud.notify"))
                    all.sendMessage(BridgeClient.PREFIX + "Service §e" + event.getService().getName() + "§7 is now §aregistered§7.");
            }
        });
        AunaCloudAPI.getEventAPI().registerEventListener(ServerUnregisterEvent.class, event -> {
            for (Player all : Bukkit.getOnlinePlayers()) {
                if (all.hasPermission("auna.cloud.notify"))
                    all.sendMessage(BridgeClient.PREFIX + "Service §e" + event.getService().getName() + "§7 is now §cunregistered§7.");
            }
        });
        AunaCloudAPI.getEventAPI().registerEventListener(ServerStartingEvent.class, event -> {
            for (Player all : Bukkit.getOnlinePlayers()) {
                if (all.hasPermission("auna.cloud.notify"))
                    all.sendMessage(BridgeClient.PREFIX + "Service §e" + event.getServiceName() + "§7 is now §6starting§7.");

            }
        });

        AunaCloudAPI.getEventAPI().registerEventListener(ServerTimeOutEvent.class, event -> {
            for (Player all : Bukkit.getOnlinePlayers()) {
                if (all.hasPermission("auna.cloud.notify"))
                    all.sendMessage(BridgeClient.PREFIX + "Service §e" + event.getServiceName() + "§4 timed out§7!");
            }
        });
    }

}
