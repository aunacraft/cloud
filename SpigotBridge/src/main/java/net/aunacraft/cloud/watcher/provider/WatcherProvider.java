package net.aunacraft.cloud.watcher.provider;

import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.watcher.Watcher;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class WatcherProvider {

    private List<Watcher> watchers;

    public WatcherProvider() {
        this.watchers = new CopyOnWriteArrayList<>();
    }

    public Watcher getOrCreate(Service service){
        Watcher watcher = watchers.stream().filter(watcher1 -> watcher1.getService().equals(service)).findAny().orElse(null);
        if(watcher == null){
            Watcher newWatcher = new Watcher(service);
            watchers.add(newWatcher);
            watcher = newWatcher;
        }
        return watcher;
    }
}
