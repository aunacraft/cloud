package net.aunacraft.cloud.watcher;

import lombok.Getter;
import net.aunacraft.cloud.CloudPlugin;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

public class Watcher {

    public int[] array;
    @Getter
    private BukkitTask bukkitTask;
    @Getter
    private Service service;

    public Watcher(Service service) {
        array = new int[]{
                -1, -1, -1, -1, -1, -1, -1, -1, -1
        };
        bukkitTask = Bukkit.getScheduler().runTaskTimer(CloudPlugin.getInstance(), () -> {
            for(int i = 0; i < 8; i++)
                array[i] = array[i+1];
            array[8] = service.getCurrentMemory();
        }, 0, 20);
        this.service = service;
    }

    public void close(){
        Bukkit.getScheduler().cancelTask(bukkitTask.getTaskId());
    }
}
