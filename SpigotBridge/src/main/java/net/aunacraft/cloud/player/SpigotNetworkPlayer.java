package net.aunacraft.cloud.player;

import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutPlayerExecuteCommand;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutSendPlayer;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SpigotNetworkPlayer extends AbstractNetworkPlayer {

    public SpigotNetworkPlayer(UUID uuid, String name, RunningService currentService) {
        super(uuid, name, currentService);
    }

    @Override
    public void executeCommand(String command) {
        Player player = Bukkit.getPlayer(this.getName());
        player.performCommand(command);
    }

    @Override
    public void sendToService(Service service) {
        BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutSendPlayer(getUUID(), service.getName()));
    }

    @Override
    public void executeBungeeCommand(String command) {
        BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutPlayerExecuteCommand(this.getUUID(), command, false));
    }

    @Override
    public void executeSpigotCommand(String command) {
        executeCommand(command);
    }
}
