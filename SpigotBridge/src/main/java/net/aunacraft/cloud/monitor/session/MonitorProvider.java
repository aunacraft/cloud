package net.aunacraft.cloud.monitor.session;

import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
public class MonitorProvider {

    private List<Player> currentlyInSession;

    public MonitorProvider() {
        this.currentlyInSession = new CopyOnWriteArrayList<>();
    }

    public void add(Player player){
        currentlyInSession.add(player);
    }

    public void remove(Player player){
        currentlyInSession.remove(player);
    }

    public void call(String log){
        currentlyInSession.forEach(player -> player.sendMessage("§cConsole §7| §f" + log));
    }
}
