package net.aunacraft.cloud.monitor;

import lombok.Getter;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.core.filter.AbstractFilterable;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Getter
public class LogAppender extends AbstractFilter {

    private Consumer<String> logConsumer;

    public LogAppender(Consumer<String> logConsumer) {
        Map<?, ?> appenders = ((org.apache.logging.log4j.core.Logger)(LogManager.getRootLogger())).getAppenders();
        List<Appender> filterable = (List<Appender>) appenders.values().stream().filter(appender -> appender instanceof AbstractFilterable).collect(Collectors.toList());
        filterable.forEach(appender -> ((AbstractFilterable)(appender)).addFilter(this));
        this.logConsumer = logConsumer;
    }

    public Filter.Result filter(LogEvent event) {
        String message = event.getMessage().getFormattedMessage();

        if(logConsumer != null)
            logConsumer.accept(message);

        if(event.getLevel().equals(Level.ERROR) || event.getLevel().equals(Level.FATAL))
            Bukkit.getOnlinePlayers().stream().filter(player -> player.hasPermission("cloud.exceptionnotify")).forEach(player ->
                player.sendMessage(BridgeClient.PREFIX + "§7An §4Exception §7occurred! Info: §c"
                        + (event.getThrown() != null ? event.getThrown().toString() : "Not Gettable")));

        if (message.indexOf(36) != -1 && message.toLowerCase(Locale.ROOT).contains("ldap"))
            return Filter.Result.DENY;

        return super.filter(event);
    }
}
