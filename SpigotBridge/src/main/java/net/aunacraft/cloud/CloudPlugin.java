package net.aunacraft.cloud;

import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.BridgeClient;
import net.aunacraft.cloud.bridgeclient.loader.SimpleBridgeLoader;
import net.aunacraft.cloud.bridgeclient.network.packets.out.PacketOutUnregisterService;
import net.aunacraft.cloud.bridgeclient.network.player.AbstractNetworkPlayer;
import net.aunacraft.cloud.bridgeclient.services.group.ServiceGroupType;
import net.aunacraft.cloud.bridgeclient.services.service.RunningService;
import net.aunacraft.cloud.bridgeclient.services.service.Service;
import net.aunacraft.cloud.commands.CloudCommand;
import net.aunacraft.cloud.listener.InventoryListener;
import net.aunacraft.cloud.listener.MessageListener;
import net.aunacraft.cloud.listener.PlayerConnectionListener;
import net.aunacraft.cloud.monitor.LogAppender;
import net.aunacraft.cloud.monitor.session.MonitorProvider;
import net.aunacraft.cloud.player.SpigotNetworkPlayer;
import net.aunacraft.cloud.watcher.Watcher;
import net.aunacraft.cloud.watcher.provider.WatcherProvider;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Getter
public class CloudPlugin extends JavaPlugin {

    private LogAppender logAppender;
    private MonitorProvider monitorProvider;
    @Getter
    private static CloudPlugin instance;
    private WatcherProvider watcherProvider;

    @Override
    public void onEnable() {
        // CLOUD STUFF
        instance = this;
        new BridgeClient(new Loader());
        new MessageListener();

        this.watcherProvider = new WatcherProvider();
        this.monitorProvider = new MonitorProvider();
        this.logAppender = new LogAppender(this.monitorProvider::call);

        //REGISTER COMMANDS
        new CloudCommand(this.getCommand("cloud"));
        //BUKKIT LISTENERS

        Stream.of(
                new InventoryListener(),
                new PlayerConnectionListener()
        ).forEach(listener -> this.getServer().getPluginManager().registerEvents(listener, this));

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            BridgeClient.getInstance().sendUpdatePacket();
        }, 20, 20);
    }

    @Override
    public void onDisable() {
        BridgeClient.getInstance().getNetworkManager().sendPacket(new PacketOutUnregisterService(AunaCloudAPI.getServiceAPI().getLocalService().getName()));
    }

    private static class Loader extends SimpleBridgeLoader {

        public Loader() {
            super(Bukkit.getPort(), ServiceGroupType.SPIGOT);
        }

        @Override
        public int getOnlinePlayerAmount() {
            return Bukkit.getOnlinePlayers().size();
        }

        @Override
        public void stopService() {
            Bukkit.shutdown();
        }

        @Override
        public void addService(Service service) {
            BridgeClient.LOGGER.info("Registered service '" + service.getName() + "'");
        }

        @Override
        public void removeService(Service service) {
        }

        @Override
        public AbstractNetworkPlayer createPlayerInstance(UUID uuid, String name, RunningService service) {
            return new SpigotNetworkPlayer(uuid, name, service);
        }

        @Override
        public void registered(RunningService localService) {
            if (localService.getGroup().isStatic()) {
                //disable autosave
                Bukkit.getWorlds().forEach(world -> world.setAutoSave(false));

            }
        }

    }
}
