package de.febanhd.fcommand.parser.impl;

import de.febanhd.fcommand.executor.CommandExecutor;
import de.febanhd.fcommand.parser.ParameterParser;

public class StringParameterParser implements ParameterParser<String> {

    @Override
    public boolean isValid(String in, CommandExecutor executor) {
        return true;
    }

    @Override
    public String convertString(String string) {
        return string;
    }
}
