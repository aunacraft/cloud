package de.febanhd.fcommand.parser.impl;

import de.febanhd.fcommand.executor.CommandExecutor;

public class PositiveDoubleParser extends DoubleParameterParser {

    @Override
    public boolean isValid(String in, CommandExecutor executor) {
        if(super.isValid(in, executor)) {
            double i = parse(in);
            if(i < 0) return false;
            return true;
        }else
            return false;
    }
}
