package de.febanhd.fcommand.parser.impl;

public class DoubleParameterParser extends SerializeAbleParameterParser<Double> {

    @Override
    public Double parse(String in) {
        return Double.parseDouble(in);
    }
}
