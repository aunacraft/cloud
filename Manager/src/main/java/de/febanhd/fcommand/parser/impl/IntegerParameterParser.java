package de.febanhd.fcommand.parser.impl;

public class IntegerParameterParser extends SerializeAbleParameterParser<Integer> {

    @Override
    public Integer parse(String in) {
        return Integer.parseInt(in);
    }
}
