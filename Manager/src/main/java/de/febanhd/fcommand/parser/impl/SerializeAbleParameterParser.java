package de.febanhd.fcommand.parser.impl;

import de.febanhd.fcommand.executor.CommandExecutor;
import de.febanhd.fcommand.parser.ParameterParser;

import java.io.Serializable;

public abstract class SerializeAbleParameterParser<T extends Serializable> implements ParameterParser<T> {


    @Override
    public boolean isValid(String in, CommandExecutor executor) {
        try {
            parse(in);
            return true;
        }catch (Exception e) {
            return false;
        }
    }

    @Override
    public T convertString(String string) {
        return parse(string);
    }

    public abstract T parse(String in);
}
