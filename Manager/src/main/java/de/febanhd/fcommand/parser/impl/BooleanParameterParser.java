package de.febanhd.fcommand.parser.impl;

public class BooleanParameterParser extends SerializeAbleParameterParser<Boolean> {

    @Override
    public Boolean parse(String in) {
        return Boolean.parseBoolean(in);
    }
}
