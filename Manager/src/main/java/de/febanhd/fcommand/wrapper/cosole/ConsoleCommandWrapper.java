package de.febanhd.fcommand.wrapper.cosole;

import de.febanhd.fcommand.Command;
import de.febanhd.fcommand.FCommandManager;
import de.febanhd.fcommand.wrapper.CommandWrapper;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.MaskingCallback;
import org.jline.reader.UserInterruptException;
import org.jline.reader.impl.DefaultParser;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.io.IOException;
import java.nio.charset.Charset;

public class ConsoleCommandWrapper implements CommandWrapper {

    private final LineReader reader;
    private FCommandManager commandManager;

    public ConsoleCommandWrapper() {
        TerminalBuilder terminalBuilder = TerminalBuilder.builder();
        terminalBuilder.encoding(Charset.defaultCharset());
        terminalBuilder.system(true);
        Terminal terminal = null;
        try {
            terminal = terminalBuilder.build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LineReader reader = LineReaderBuilder.builder()
                .terminal(terminal)
                .parser(new DefaultParser())
                .appName("AunaCloud")
                .build();
        this.reader = reader;

        new Thread(() -> {
            while(true) {
                this.update();
            }
        }, "Console-Reader").start();

    }

    @Override
    public void register(Command command, FCommandManager commandManager) {
        this.commandManager = commandManager;
    }

    public void update() {
        if (reader == null) return;
        String line = null;
        try {
            line = reader.readLine("> ", null, (MaskingCallback) null, null);
        }catch (UserInterruptException e) {
            if(e.getPartialLine().isEmpty()) {
                System.exit(1);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        if(line == null) return;
        line = line.trim();
        if(line.isEmpty()) return;
        commandManager.execute(message -> System.out.println("> " + message), line);
    }
}
