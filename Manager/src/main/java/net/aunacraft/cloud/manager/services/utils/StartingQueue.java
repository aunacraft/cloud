package net.aunacraft.cloud.manager.services.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;

import java.util.*;
import java.util.concurrent.ConcurrentMap;

public class StartingQueue {

    private final ConcurrentMap<ServiceGroup, Integer> QUEUE = Maps.newConcurrentMap();
    private long lastStart = 0;

    public void add(ServiceGroup serviceGroup) {
        if (!QUEUE.containsKey(serviceGroup)) {
            QUEUE.put(serviceGroup, 1);
        } else {
            QUEUE.put(serviceGroup, QUEUE.get(serviceGroup) + 1);
        }
    }

    public void tick() {
        startNewServiceIfAvailable();
    }

    private void startNewServiceIfAvailable() {
        if (lastStart + 5000 > System.currentTimeMillis()) return;
        ServiceGroup group = getGroupWithHighestPriority();
        if (group == null) return;

        Runtime runtime = Runtime.getRuntime();
        long availableMemory = runtime.totalMemory() - runtime.freeMemory();
        if (group.getMaxMemory() > availableMemory) {
            CloudManager.LOGGER.warn("Cant start service from group " + group.getGroupName() + " beacause it needs to much memory");
        } else {
            int i = QUEUE.get(group);
            if (i == 1)
                QUEUE.remove(group);
            else
                QUEUE.put(group, i - 1);
            group.startNewService();
            this.lastStart = System.currentTimeMillis();
        }

    }

    private ServiceGroup getGroupWithHighestPriority() {
        return Lists.newArrayList(QUEUE.keySet()).stream().sorted(Comparator.comparing(ServiceGroup::getPriority)).findFirst().orElse(null);
    }

    public int getServiceAmountInQueue(ServiceGroup group) {
        return this.QUEUE.getOrDefault(group, 0);
    }
}
