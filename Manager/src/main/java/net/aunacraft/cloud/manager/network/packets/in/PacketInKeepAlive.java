package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.NetworkManager;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;

public class PacketInKeepAlive implements Packet {

    private int id;

    public PacketInKeepAlive(int id) {
        this.id = id;
    }

    public PacketInKeepAlive() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        byteBuf.writeInt(this.id);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.id = byteBuf.readInt();
    }

    @Override
    public void handle(PacketListener networkManager) {
        
    }
}
