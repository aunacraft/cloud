package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

import java.util.UUID;

public class PacketOutSendPlayer implements Packet {

    private UUID uuid;
    private String serviceName;

    public PacketOutSendPlayer(UUID uuid, String serviceName) {
        this.uuid = uuid;
        this.serviceName = serviceName;
    }

    public PacketOutSendPlayer() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.serviceName = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(PacketListener packetListener) {

    }
}
