package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.service.RunningService;

import java.util.UUID;

public class PacketInAddPlayer implements Packet {

    private UUID uuid;
    private String name;
    private String service;

    public PacketInAddPlayer(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public PacketInAddPlayer() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, name);
        ByteBufUtils.writeString(byteBuf, service);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.name = ByteBufUtils.readString(byteBuf);
        this.service = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(PacketListener packetListener) {
        RunningService service = CloudManager.getInstance().getServiceManager().getGroupCache().getService(this.service);
        NetworkPlayer player = new NetworkPlayer(uuid, name, service);
        PlayerCache.addPlayer(player);
    }
}
