package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;

import java.util.List;

public class PacketOutRegisterService implements Packet {

    private RunningService localService;
    private RunningService proxyService;
    private String fallbackGroupName;
    private long ping = System.currentTimeMillis();

    public PacketOutRegisterService(RunningService localService, RunningService proxyService, String fallbackGroupName) {
        this.localService = localService;
        this.proxyService = proxyService;
        this.fallbackGroupName = fallbackGroupName;

    }

    public PacketOutRegisterService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeService(byteBuf, this.localService);
        ByteBufUtils.writeService(byteBuf, this.proxyService);
        ByteBufUtils.writeString(byteBuf, this.fallbackGroupName);
        byteBuf.writeLong(ping);
    }

    @Override
    public void read(ByteBuf byteBuf) {

    }

    @Override
    public void handle(PacketListener packetListener) {

    }
}
