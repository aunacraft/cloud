package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

import java.util.List;

public class PacketOutRegisterPlayers implements Packet {

    private List<NetworkPlayer> players;

    public PacketOutRegisterPlayers(List<NetworkPlayer> players) {
        this.players = players;
    }

    public PacketOutRegisterPlayers() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writePlayerList(byteBuf, players);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.players = ByteBufUtils.readPlayerList(byteBuf);
    }

    @Override
    public void handle(PacketListener packetListener) {

    }
}
