package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

@Getter
public class PacketInStartSpigotService implements Packet {

    private String groupName;

    public PacketInStartSpigotService(String groupName) {
        this.groupName = groupName;
    }

    public PacketInStartSpigotService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.groupName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.groupName = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(PacketListener packetListener) {
        packetListener.handleStartServicePacket(this);
    }
}
