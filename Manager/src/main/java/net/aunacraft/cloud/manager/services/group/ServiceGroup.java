package net.aunacraft.cloud.manager.services.group;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.file.Directory;
import net.aunacraft.cloud.manager.file.FileSystem;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;
import net.aunacraft.cloud.manager.services.utils.PortHelper;
import net.aunacraft.cloud.manager.services.utils.StartingQueue;
import net.aunacraft.cloud.manager.utils.FileUtils;
import net.aunacraft.cloud.manager.utils.SHUtils;
import org.checkerframework.checker.units.qual.C;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;
import java.util.jar.JarFile;

@Getter
public abstract class ServiceGroup {

    private final String groupName;
    private final String displayName;
    private final boolean isStatic;
    private final int maxOnlineServices;
    private final int minOnlineServices;
    private int maxPlayers;
    private final Directory directory;
    private final int maxMemory;
    private final List<RunningService> onlineServices = Lists.newCopyOnWriteArrayList();
    private final ServiceGroupType type;
    private final int priority;

    private final ServiceLoadBalancer loadBalancer = new ServiceLoadBalancer(this);

    private final List<RunningService> serviceRemoveQueue = Lists.newArrayList();

    public ServiceGroup(String groupName, String displayName, boolean isStatic, int maxOnlineServices, int minOnlineServices, int maxPlayers, Directory directory, int maxMemory, ServiceGroupType type, int priority) {
        this.groupName = groupName;
        this.priority = priority;
        this.displayName = displayName;
        this.isStatic = isStatic;
        this.maxOnlineServices = maxOnlineServices;
        this.minOnlineServices = minOnlineServices;
        this.maxPlayers = maxPlayers;
        this.directory = directory;
        this.maxMemory = maxMemory;
        this.type = type;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        this.onlineServices.forEach(service -> service.setMaxPlayers(maxPlayers));
    }

    public void startNewService() {
        if(!CloudManager.getInstance().getServiceManager().canStartService(this.maxMemory)) {
            CloudManager.LOGGER.warn("Cant start new service of group '" + this.groupName + "' beacause there is too little memory");
            return;
        }
        FileSystem fileSystem = CloudManager.getInstance().getFileSystem();
        String identifier = UUID.randomUUID().toString().substring(0, 5);
        int id = 0;
        String name;
        do {
            id++;
            name = this.getGroupName() + "-" + id;
        }while (isUsedServiceName(name));
        int port = type == ServiceGroupType.PROXY ? 25565 : PortHelper.nextPort();

        CloudManager.LOGGER.info("Starting service '" + name + "' on port " + port);

        File workingDir = !isStatic ? new File(fileSystem.getTempDirectory().getDirFile(), name + "_" + identifier) :
                this.directory.getDirFile();

        File jarFile = new File(workingDir, type.getFinalJarName());
        if (!jarFile.exists()) {
            jarFile.delete();
        }

        try {

            if (!isStatic) {
                if (workingDir.exists())
                    FileUtils.deleteDirectoryAndContents(workingDir.toPath());
                workingDir.mkdir();
                FileUtils.copyFolder(directory.getDirFile().toPath(), workingDir.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }

            if (type == ServiceGroupType.SPIGOT)
                FileUtils.copyFolder(fileSystem.getSpigotAllDir().getDirFile().toPath(), workingDir.toPath(), StandardCopyOption.REPLACE_EXISTING);

            jarFile.createNewFile();
            Files.copy(new File(fileSystem.getJarsDirectory().getDirFile(),
                            type.getDefaultJarName()).toPath(),
                    jarFile.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);

            File pluginDir = new File(workingDir, "plugins");
            if (!pluginDir.exists())
                pluginDir.mkdir();

            fileSystem.copyCloudBridgePlugin(pluginDir, type);

        } catch (IOException e) {
            CloudManager.LOGGER.error("Cannot create temp dir for service " + name, e);
        }

        synchronized (onlineServices) {
            try {
                onlineServices.add(createService(name, id, identifier, port, jarFile, workingDir));
            } catch (Exception e) {
                CloudManager.LOGGER.error("Cant start service '" + name + "'", e);
            }
        }

    }

    private boolean isUsedServiceName(String name) {
        for (Service onlineService : this.onlineServices) {
            if (onlineService.getName().equals(name)) return true;
        }
        return false;
    }

    public final boolean needNewService(StartingQueue startingQueue) {
        return this.loadBalancer.needNewService(startingQueue);
    }

    public abstract RunningService createService(String name, int id, String identifier, int port, File jarFile, File workingDir) throws IOException;

    public void sendPacketToOnlineServices(Service sender, Packet packet) {
        this.onlineServices.forEach(runningService -> {
            if(runningService.isRunning() && (sender == null || runningService != sender)) {
                runningService.getNetworkManager().sendPacket(packet);
            }
        });
    }

    public void removeService(RunningService service) {
        synchronized (serviceRemoveQueue) {
            if (this.onlineServices.contains(service)) {
                this.serviceRemoveQueue.add(service);
            }
        }
    }

    public void tick() {
        synchronized (serviceRemoveQueue) {
            this.onlineServices.removeAll(this.serviceRemoveQueue);
            this.serviceRemoveQueue.clear();
        }
        this.loadBalancer.tick();
    }
}
