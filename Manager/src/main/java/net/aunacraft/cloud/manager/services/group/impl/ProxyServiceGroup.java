package net.aunacraft.cloud.manager.services.group.impl;

import lombok.Getter;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.ServiceGroupType;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;
import net.aunacraft.cloud.manager.utils.SHUtils;

import java.io.File;
import java.io.IOException;

@Getter
public class ProxyServiceGroup extends ServiceGroup {

    private final ServiceGroup fallbackGroup;
    private RunningService service;

    public ProxyServiceGroup(ServiceGroup fallbackGroup, int maxOnlinePlayers) {
        super("Bungee", "Bungee", true, 1, 1, maxOnlinePlayers,
                CloudManager.getInstance().getFileSystem().createGroupDirectory("Bungee", true),
                CloudManager.getInstance().getFileSystem().getConfigDirectory().getDefaultConfig().getBungeeCordMemory(), ServiceGroupType.PROXY, 0);
        this.fallbackGroup = fallbackGroup;
    }

    @Override
    public RunningService createService(String name, int id, String identifier, int port, File jarFile, File workingDir) throws IOException {
        String screenName = name + "_" + identifier;
        SHUtils.executeCommand(
                "screen -mdS " + screenName +
                        " /bin/sh -c '" +
                        "cd " + workingDir.getAbsolutePath() + " &&" +
                        " java" +
                        " -Xms" + getMaxMemory() + "M" +
                        " -Xmx" + this.getMaxMemory() + "M" +
                        " -XX:+UseG1GC" +
                        " -XX:+UnlockExperimentalVMOptions" +
                        " -XX:+ParallelRefProcEnabled" +
                        " -XX:+AlwaysPreTouch" +
                        " -XX:G1HeapRegionSize=8M" +
                        " -XX:G1ReservePercent=20" +
                        " -XX:G1HeapWastePercent=5" +
                        " -XX:G1MixedGCCountTarget=4" +
                        " -XX:MaxGCPauseMillis=50" +
                        " -XX:+DisableExplicitGC" +
                        " -XX:TargetSurvivorRatio=90" +
                        " -XX:G1NewSizePercent=30" +
                        " -XX:G1MaxNewSizePercent=40" +
                        " -XX:InitiatingHeapOccupancyPercent=15" +
                        " -XX:G1MixedGCLiveThresholdPercent=90" +
                        " -XX:G1RSetUpdatingPauseTimePercent=5" +
                        " -XX:SurvivorRatio=32" +
                        " -XX:+PerfDisableSharedMem" +
                        " -XX:MaxTenuringThreshold=1" +
                        " -jar " + jarFile.getName() +
                        "'"
        );

        String displayName = this.getDisplayName().replace("%d", String.valueOf(id));
        return (this.service = new RunningService(name, this, displayName, screenName, workingDir, Service.ServiceState.STARTING, this.getMaxPlayers(), port));
    }

    public void removeAllPlayers() {
        this.service.getOnlinePlayers().forEach(PlayerCache::removePlayer);
    }
}
