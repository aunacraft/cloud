package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

import java.util.UUID;

public class PacketInPlayerExecuteCommand implements Packet {

    private UUID uuid;
    private String command;
    private boolean proxy;

    public PacketInPlayerExecuteCommand(UUID uuid, String command, boolean proxy) {
        this.uuid = uuid;
        this.command = command;
        this.proxy = proxy;
    }

    public PacketInPlayerExecuteCommand() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, command);
        byteBuf.writeBoolean(this.proxy);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.command = ByteBufUtils.readString(byteBuf);
        this.proxy = byteBuf.readBoolean();
    }

    @Override
    public void handle(PacketListener packetListener) {
        NetworkPlayer player = PlayerCache.getPlayer(this.uuid);
        if(proxy)
            player.executeSpigotCommand(this.command);
        else
            player.executeBungeeCommand(this.command);
    }
}
