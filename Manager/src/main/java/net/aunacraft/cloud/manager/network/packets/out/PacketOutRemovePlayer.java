package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

import java.util.UUID;

public class PacketOutRemovePlayer implements Packet {

    private UUID uuid;

    public PacketOutRemovePlayer(UUID uuid) {
        this.uuid = uuid;
    }

    public PacketOutRemovePlayer() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
    }

    @Override
    public void handle(PacketListener packetListener) {
        NetworkPlayer player = PlayerCache.getPlayer(uuid);
        PlayerCache.removePlayer(player);
    }
}
