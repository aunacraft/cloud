package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;

import java.util.List;

public class PacketOutAddServiceGroup implements Packet {

    private List<ServiceGroup> groups;

    public PacketOutAddServiceGroup(List<ServiceGroup> groups) {
        this.groups = groups;
    }

    public PacketOutAddServiceGroup() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeServiceGroupList(byteBuf, groups);
    }

    @Override
    public void read(ByteBuf byteBuf) {

    }

    @Override
    public void handle(PacketListener packetListener) {

    }
}
