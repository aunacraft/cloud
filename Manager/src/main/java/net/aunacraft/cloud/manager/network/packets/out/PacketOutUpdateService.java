package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.service.RunningService;

@Getter
public class PacketOutUpdateService implements Packet {

    private RunningService service;

    public PacketOutUpdateService(RunningService service) {
        this.service = service;
    }

    public PacketOutUpdateService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeService(byteBuf, service);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.service = ByteBufUtils.readService(byteBuf);
    }

    @Override
    public void handle(PacketListener packetListener) {

    }
}
