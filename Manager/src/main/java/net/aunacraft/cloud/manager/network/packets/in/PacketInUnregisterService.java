package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.service.RunningService;

@Getter
public class PacketInUnregisterService implements Packet {

    private String serviceName;

    public PacketInUnregisterService(String serviceName) {
        this.serviceName = serviceName;
    }

    public PacketInUnregisterService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.serviceName = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(PacketListener packetListener) {
        RunningService service = CloudManager.getInstance().getServiceManager().getGroupCache().getService(this.serviceName);
        if(service != null) {
            service.stop();
        }
    }
}
