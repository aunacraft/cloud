package net.aunacraft.cloud.manager.commands;

import de.febanhd.fcommand.FCommand;
import de.febanhd.fcommand.builder.CommandBuilder;
import de.febanhd.fcommand.builder.ParameterBuilder;
import net.aunacraft.cloud.manager.commands.parser.ServiceParameterParser;
import net.aunacraft.cloud.manager.services.service.RunningService;

public class StopServiceCommand implements FCommand {

    @Override
    public CommandBuilder create(CommandBuilder builder) {
        return builder
                .parameter(
                        ParameterBuilder.beginParameter("service")
                                .required()
                                .parser(new ServiceParameterParser())
                                .build()
                )
                .handler((executor, ctx) -> {
                    RunningService service = ctx.getParameterValue("service", RunningService.class);
                    service.stop();
                })
                ;
    }
}
