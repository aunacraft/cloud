package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.service.RunningService;

@Getter
public class PacketInUpdateService implements Packet {

    private RunningService updatedService;

    public PacketInUpdateService(RunningService service) {
        this.updatedService = service;
    }

    public PacketInUpdateService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeService(byteBuf, updatedService);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.updatedService = ByteBufUtils.readService(byteBuf);
    }

    @Override
    public void handle(PacketListener packetListener) {
        RunningService service = CloudManager.getInstance().getServiceManager().getGroupCache().getService(updatedService.getName());
        if(service != null && service.getNetworkManager() != null && service.getNetworkManager().getPacketListener() != null) {
            service.getNetworkManager().getPacketListener().handleUpdateServicePacket(this);
        }
    }
}
