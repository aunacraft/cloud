package net.aunacraft.cloud.manager.commands;

import de.febanhd.fcommand.FCommand;
import de.febanhd.fcommand.builder.CommandBuilder;
import de.febanhd.fcommand.builder.ParameterBuilder;
import de.febanhd.fcommand.parser.impl.BooleanParameterParser;
import de.febanhd.fcommand.parser.impl.PositiveIntegerParser;
import de.febanhd.fcommand.parser.impl.StringParameterParser;
import net.aunacraft.cloud.manager.CloudManager;

public class CreateGroupCommand implements FCommand {

    @Override
    public CommandBuilder create(CommandBuilder builder) {
        return builder
                .parameter(
                        ParameterBuilder.beginParameter("groupName")
                                .parser(new StringParameterParser())
                                .required()
                                .build()
                )
                .parameter(
                        ParameterBuilder.beginParameter("displayName")
                                .parser(new StringParameterParser())
                                .required()
                                .build()
                )
                .parameter(
                        ParameterBuilder.beginParameter("isStatic")
                                .parser(new BooleanParameterParser())
                                .required()
                                .build()
                )
                .parameter(
                        ParameterBuilder.beginParameter("minOnlineServices")
                                .parser(new PositiveIntegerParser())
                                .required()
                                .build()
                )
                .parameter(
                        ParameterBuilder.beginParameter("maxOnlineServices")
                                .parser(new PositiveIntegerParser())
                                .required()
                                .build()
                )
                .parameter(
                        ParameterBuilder.beginParameter("maxPlayers")
                                .parser(new PositiveIntegerParser())
                                .required()
                                .build()
                )
                .parameter(
                        ParameterBuilder.beginParameter("memory")
                                .parser(new PositiveIntegerParser())
                                .required()
                                .build()
                )
                .parameter(
                        ParameterBuilder.beginParameter("priority")
                                .parser(new PositiveIntegerParser())
                                .required()
                                .build()
                )

                .handler((executor, ctx) -> {
                    String groupName = ctx.getParameterValue("groupName", String.class);
                    String displayName = ctx.getParameterValue("displayName", String.class);
                    boolean isStatic = ctx.getParameterValue("isStatic", Boolean.class);
                    int minOnlineServices = ctx.getParameterValue("minOnlineServices", Integer.class);
                    int maxOnlineServices = ctx.getParameterValue("maxOnlineServices", Integer.class);
                    int maxPlayers = ctx.getParameterValue("maxPlayers", Integer.class);
                    int memory = ctx.getParameterValue("memory", Integer.class);
                    int priority = ctx.getParameterValue("priority", Integer.class);

                    if(CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroup(groupName) != null)
                        executor.sendMessage("A group with the name '" + groupName + "' already exist!");
                    else {
                        CloudManager.getInstance().getServiceManager().createServiceGroup(groupName, displayName, isStatic, minOnlineServices, maxOnlineServices, maxPlayers, memory, priority);
                        executor.sendMessage("Created group '" + groupName + "'");
                    }
                })
                ;
    }
}
