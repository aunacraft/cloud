package net.aunacraft.cloud.manager.network.player;

import lombok.Getter;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutPlayerExecuteCommand;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutPlayerSwitchService;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutRemovePlayer;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutSendPlayer;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;

import java.util.UUID;

@Getter
public class NetworkPlayer {

    private final UUID uuid;
    private final String name;
    private RunningService currentService;

    public NetworkPlayer(UUID uuid, String name, RunningService currentService) {
        this.uuid = uuid;
        this.name = name;
        this.currentService = currentService;
    }

    public void executeBungeeCommand(String command) {
        CloudManager.getInstance().getServiceManager().getGroupCache().getProxyServiceGroup().getService().getNetworkManager()
                .sendPacketDirect(new PacketOutPlayerExecuteCommand(getUuid(), command));
    }

    public void executeSpigotCommand(String command) {
        this.currentService.getNetworkManager().sendPacketDirect(new PacketOutPlayerExecuteCommand(getUuid(), command));
    }

    public void sendToService(String serviceName) {
        CloudManager.getInstance().getServiceManager().getGroupCache().getProxyServiceGroup().getService().getNetworkManager()
                .sendPacket(new PacketOutSendPlayer(getUuid(), serviceName));
    }

    public void setCurrentService(RunningService service) {
        this.currentService = service;
        sendPacketToAllServices(new PacketOutPlayerSwitchService(this.uuid, this.currentService.getName()));
    }

    public void sendPacketToAllServices(Packet packet) {
        for (ServiceGroup serviceGroup : CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroups()) {
            serviceGroup.sendPacketToOnlineServices(null, packet);
        }
    }
}
