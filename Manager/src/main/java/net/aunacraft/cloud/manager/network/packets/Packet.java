package net.aunacraft.cloud.manager.network.packets;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.NetworkManager;
import net.aunacraft.cloud.manager.network.manager.PacketListener;

public interface Packet {

    void write(ByteBuf byteBuf);

    void read(ByteBuf byteBuf);

    void handle(PacketListener packetListener);
}
