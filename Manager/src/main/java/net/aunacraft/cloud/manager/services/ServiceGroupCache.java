package net.aunacraft.cloud.manager.services;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.file.FileSystem;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.impl.ProxyServiceGroup;
import net.aunacraft.cloud.manager.services.group.impl.SpigotServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;
import net.aunacraft.cloud.manager.services.utils.PortHelper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
public class ServiceGroupCache {

    private final ProxyServiceGroup proxyServiceGroup;
    private final CopyOnWriteArrayList<SpigotServiceGroup> spigotServiceGroups;
    private final SpigotServiceGroup fallbackGroup;

    public ServiceGroupCache() {
        FileSystem fileSystem = CloudManager.getInstance().getFileSystem();
        this.spigotServiceGroups = Lists.newCopyOnWriteArrayList();

        fileSystem.getGroupDirectory().getFiles().stream().filter(file -> file.getName().endsWith(".json")).forEach(file -> {
            try {
                spigotServiceGroups.add(readFromFile(file));
            } catch (Exception e) {
                CloudManager.LOGGER.error("Cannot load group from file " + file.getName(), e);
            }
        });

        String fallbackGroupName = fileSystem.getConfigDirectory().getDefaultConfig().getFallbackGroup();
        SpigotServiceGroup fallbackGroup = getSpigotServiceGroup(fallbackGroupName);
        if (fallbackGroup == null) {
            CloudManager.LOGGER.warn("Fallback group does not exist. Create it...");
            fallbackGroup = new SpigotServiceGroup(fallbackGroupName, fallbackGroupName + "-%d", false, 3, 1, 25, 2000, 0);
            addSpigotGroup(fallbackGroup);
        }
        this.fallbackGroup = fallbackGroup;
        this.proxyServiceGroup = new ProxyServiceGroup(fallbackGroup, fileSystem.getConfigDirectory().getDefaultConfig().getMaxPlayers());
    }

    public void addSpigotGroup(SpigotServiceGroup serviceGroup) {
        this.spigotServiceGroups.add(serviceGroup);
        safeInFile(serviceGroup);
    }

    public SpigotServiceGroup readFromFile(File file) throws IOException {
        HashMap<String, Object> json = FileSystem.GSON.fromJson(new String(Files.readAllBytes(file.toPath())), HashMap.class);

        return new SpigotServiceGroup(
                (String) json.get("groupName"),
                (String) json.get("displayName"),
                (boolean) json.get("static"),
                Integer.parseInt(json.get("maxOnlineServices").toString().split("\\.")[0]),
                Integer.parseInt(json.get("minOnlineServices").toString().split("\\.")[0]),
                Integer.parseInt(json.get("maxPlayers").toString().split("\\.")[0]),
                Integer.parseInt(json.get("maxMemory").toString().split("\\.")[0]),
                Integer.parseInt(json.get("priority").toString().split("\\.")[0]));
    }

    @SneakyThrows
    public void safeInFile(SpigotServiceGroup group) {
        HashMap<String, Object> json = Maps.newHashMap();
        json.put("groupName", group.getGroupName());
        json.put("displayName", group.getDisplayName());
        json.put("static", group.isStatic());
        json.put("maxOnlineServices", group.getMaxOnlineServices());
        json.put("minOnlineServices", group.getMinOnlineServices());
        json.put("maxPlayers", group.getMaxPlayers());
        json.put("maxMemory", group.getMaxMemory());
        json.put("priority", group.getPriority());

        CloudManager.getInstance().getFileSystem().getGroupDirectory().addFile(group.getGroupName() + ".json",
                FileSystem.GSON.toJson(json));
    }

    public SpigotServiceGroup getSpigotServiceGroup(String name) {
        return this.spigotServiceGroups.stream().filter(spigotServiceGroups -> spigotServiceGroups.getGroupName().equals(name)).findAny().orElse(null);
    }

    public List<ServiceGroup> getServiceGroups() {
        List<ServiceGroup> groups = Lists.newArrayList();
        groups.addAll(this.spigotServiceGroups);
        groups.add(this.proxyServiceGroup);
        return groups;
    }

    public ServiceGroup getServiceGroup(String name) {
        return this.getServiceGroups().stream().filter(group -> group.getGroupName().equals(name)).findAny().orElse(null);
    }

    public Service getStartingService(int port) {
        if(port == PortHelper.PROXY_PORT) return this.proxyServiceGroup.getOnlineServices().get(0);
        for (SpigotServiceGroup spigotServiceGroup : this.spigotServiceGroups) {
            for (Service onlineService : spigotServiceGroup.getOnlineServices()) {
                if(onlineService.getPort() == port && onlineService.getCurrentState() == Service.ServiceState.STARTING) return onlineService;
            }
        }
        return null;
    }

    public Service getService(int port) {
        for (ServiceGroup serviceGroup : getServiceGroups()) {
            Service service = serviceGroup.getOnlineServices().stream().filter(runningService -> runningService.getPort() == port).findAny().orElse(null);
            if(service != null) return service;
        }
        return null;
    }

    public RunningService getService(String name) {
        for (ServiceGroup serviceGroup : getServiceGroups()) {
            RunningService service = serviceGroup.getOnlineServices().stream().filter(s -> s.getName().equals(name)).findAny().orElse(null);
            if(service == null) continue;
            return service;

        }
        return null;
    }
}
