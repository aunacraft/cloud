package net.aunacraft.cloud.manager;

import de.febanhd.fcommand.FCommandManager;
import de.febanhd.fcommand.wrapper.cosole.ConsoleCommandWrapper;
import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.cloud.manager.commands.*;
import net.aunacraft.cloud.manager.file.FileSystem;
import net.aunacraft.cloud.manager.network.netty.BridgeServerBootstrap;
import net.aunacraft.cloud.manager.services.ServiceManager;
import net.aunacraft.cloud.manager.services.group.impl.SpigotServiceGroup;
import net.aunacraft.cloud.manager.services.utils.PortHelper;
import net.aunacraft.cloud.manager.services.utils.StartingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter
public class CloudManager {

    @Getter
    private static CloudManager instance;
    public static final Logger LOGGER = LoggerFactory.getLogger("AunaCloud");
    public static final ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool();

    public static final int CLOUD_PORT = 3333;

    public static void main(String[] args) {
        instance = new CloudManager();
    }

    private FileSystem fileSystem;
    private ServiceManager serviceManager;

    private long lastTick;

    private FCommandManager commandManager;


    @SneakyThrows
    private CloudManager() {
        instance = this;

        if(isLocked()) {
            LOGGER.error("It seems that a cloud instance is already running.");
            System.out.printf("Start anyway? (y/n): ");
            String line = System.console().readLine();
            if(!line.equalsIgnoreCase("y"))
                System.exit(-2);
            else
                unlock();
        }
        lock();


        addShutdownHook();
        long startedAt = System.currentTimeMillis();
        LOGGER.info("Starting AunaCloud by FebanHD");
        this.fileSystem = new FileSystem();
        this.serviceManager = new ServiceManager();
        commandManager = FCommandManager.create(new ConsoleCommandWrapper());
        registerCommands();
        LOGGER.info("Registered " + commandManager.getCommands().size() + " commands");
        new BridgeServerBootstrap(CLOUD_PORT);
        LOGGER.info("Start up finished: Needed " + (System.currentTimeMillis() - startedAt) / 1000f + " seconds to start");
        lastTick = System.currentTimeMillis();
        while (true) {
            Thread.sleep(20);
            tick();
            lastTick = System.currentTimeMillis();
        }
    }

    private void registerCommands() {
        commandManager.registerCommand(new CreateGroupCommand(), "creategroup");
        commandManager.registerCommand(new StopCommand(), "stop");
        commandManager.registerCommand(new StartServiceCommand(), "startservice");
        commandManager.registerCommand(new StopServiceCommand(), "stopservice");
        commandManager.registerCommand(new RestartGroupCommand(), "restartgroup");
        commandManager.registerCommand(new RestartAllCommand(), "restartall");
    }

    private void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            unlock();
            this.serviceManager.stopAllServices();
            LOGGER.info("Shutdown AunaCloud");
        }, "Shutdown"));
    }

    public void tick() {
        this.serviceManager.tick();
        PortHelper.tick();
    }

    private boolean isLocked() {
        return getLockFile().exists() || PortHelper.isPortUsed(CLOUD_PORT);
    }

    @SneakyThrows
    private void lock() {
        getLockFile().createNewFile();
    }

    private void unlock() {
        getLockFile().delete();
    }

    private File getLockFile() {
        return new File("cloud.lock");
    }
}
