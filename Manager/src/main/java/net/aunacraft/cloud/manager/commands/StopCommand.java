package net.aunacraft.cloud.manager.commands;

import de.febanhd.fcommand.FCommand;
import de.febanhd.fcommand.builder.CommandBuilder;
import de.febanhd.fcommand.builder.ParameterBuilder;
import de.febanhd.fcommand.parser.impl.BooleanParameterParser;
import de.febanhd.fcommand.parser.impl.PositiveIntegerParser;
import de.febanhd.fcommand.parser.impl.StringParameterParser;
import net.aunacraft.cloud.manager.CloudManager;

public class StopCommand implements FCommand {

    @Override
    public CommandBuilder create(CommandBuilder builder) {
        return builder
                .handler((executor, ctx) -> System.exit(1))
                ;
    }
}
