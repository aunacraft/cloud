package net.aunacraft.cloud.manager.network.player;

import com.google.common.collect.Lists;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutAddPlayer;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutRemovePlayer;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.impl.ProxyServiceGroup;
import net.aunacraft.cloud.manager.services.group.impl.SpigotServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class PlayerCache {

    private static final CopyOnWriteArrayList<NetworkPlayer> PLAYERS = Lists.newCopyOnWriteArrayList();

    public static void addPlayer(NetworkPlayer player) {
        PLAYERS.add(player);
        player.sendPacketToAllServices(new PacketOutAddPlayer(player.getUuid(), player.getName(), player.getCurrentService().getName()));
    }

    public static void removePlayer(NetworkPlayer player) {
        PLAYERS.remove(player);
        player.sendPacketToAllServices(new PacketOutRemovePlayer(player.getUuid()));
    }

    public static NetworkPlayer getPlayer(String name) {
        return PLAYERS.stream().filter(player -> player.getName().equals(name)).findAny().orElse(null);
    }

    public static NetworkPlayer getPlayer(UUID uuid) {
        return PLAYERS.stream().filter(player -> player.getUuid().equals(uuid)).findAny().orElse(null);
    }

    public static List<NetworkPlayer> getPlayersOnService(RunningService service) {
        if(service.getGroup() instanceof ProxyServiceGroup) {
            return getOnlinePlayers();
        }
        List<NetworkPlayer> players = Lists.newArrayList();
        PLAYERS.stream().filter(player -> player.getCurrentService() != null && player.getCurrentService().equals(service)).forEach(players::add);
        return players;
    }

    public static List<NetworkPlayer> getOnlinePlayers() {
        return Lists.newArrayList(PLAYERS);
    }
}
