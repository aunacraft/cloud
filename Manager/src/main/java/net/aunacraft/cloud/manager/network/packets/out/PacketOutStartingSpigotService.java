package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

public class PacketOutStartingSpigotService implements Packet {

    private String serviceName;

    public PacketOutStartingSpigotService(String serviceName) {
        this.serviceName = serviceName;
    }

    public PacketOutStartingSpigotService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.serviceName = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(PacketListener networkManager) {

    }
}
