package net.aunacraft.cloud.manager.services.utils;

import com.google.common.collect.Lists;
import net.aunacraft.cloud.manager.CloudManager;

import java.net.Socket;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PortHelper {

    public static final int START_PORT = 45000, PROXY_PORT = 25565;
    private static final List<Integer> PORTS = Lists.newArrayList();

    private static long lastCheck = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(10);

    public static int nextPort() {
        synchronized (PORTS) {
            int port = START_PORT;
            while (PORTS.contains(port) || isPortUsed(port))
                port++;
            PORTS.add(port);
            return port;
        }
    }

    public static void tick() {
        if(lastCheck + TimeUnit.SECONDS.toMillis(10) < System.currentTimeMillis()) {
            lastCheck = System.currentTimeMillis();
            CloudManager.EXECUTOR_SERVICE.execute(() -> {
                List<Integer> not_used_ports = Lists.newArrayList();
                List<Integer> ports = Lists.newArrayList(PORTS);
                for (Integer port : ports) {
                    if (CloudManager.getInstance().getServiceManager().getGroupCache().getService(port) == null) {
                        if(!isPortUsed(port))
                            not_used_ports.add(port);
                    }
                }
                synchronized (PORTS) {
                    PORTS.removeAll(not_used_ports);
                }
            });
        }
    }

    public static boolean isPortUsed(int port) {
        try {
            Socket socket = new Socket("localhost", port);
            socket.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
