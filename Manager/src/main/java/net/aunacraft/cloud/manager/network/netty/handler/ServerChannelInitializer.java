package net.aunacraft.cloud.manager.network.netty.handler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.timeout.ReadTimeoutHandler;
import net.aunacraft.cloud.manager.network.manager.NetworkManager;
import net.aunacraft.cloud.manager.network.packets.PacketRegistry;

public class ServerChannelInitializer extends ChannelInitializer<Channel> {

    @Override
    protected void initChannel(Channel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();

        pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4));
        pipeline.addLast("decoder", new PacketDecoder(PacketRegistry.registry));

        pipeline.addLast(new LengthFieldPrepender(4));
        pipeline.addLast("encoder", new PacketEncoder(PacketRegistry.registry));

        pipeline.addLast("timeout", new ReadTimeoutHandler(15));

        pipeline.addLast("handler", new NetworkManager());

    }
}
