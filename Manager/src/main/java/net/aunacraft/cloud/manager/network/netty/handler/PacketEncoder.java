package net.aunacraft.cloud.manager.network.netty.handler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.packets.PacketRegistry;

public class PacketEncoder extends MessageToByteEncoder<Packet> {

    private final PacketRegistry packetRegistry;

    public PacketEncoder(PacketRegistry packetRegistry) {
        this.packetRegistry = packetRegistry;
    }

    protected void encode(ChannelHandlerContext ctx, Packet packet, ByteBuf byteBuf)
            throws Exception {
        int id = packetRegistry.getPacketID(packet);
        byteBuf.writeInt(id);
        packet.write(byteBuf);
    }
}
