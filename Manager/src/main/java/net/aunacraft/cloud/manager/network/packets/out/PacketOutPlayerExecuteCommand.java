package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

import java.util.UUID;

public class PacketOutPlayerExecuteCommand implements Packet {

    private UUID uuid;
    private String command;

    public PacketOutPlayerExecuteCommand(UUID uuid, String command) {
        this.uuid = uuid;
        this.command = command;
    }

    public PacketOutPlayerExecuteCommand() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, command);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.command = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(PacketListener packetListener) {

    }
}
