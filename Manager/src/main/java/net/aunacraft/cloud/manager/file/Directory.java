package net.aunacraft.cloud.manager.file;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.cloud.manager.utils.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

@Getter
public class Directory {

    private final File dirFile;
    private final List<Directory> directories;
    private final Directory parentDirectory;

    public Directory(String path, Directory parentDirectory) {
        this.dirFile = new File(path);
        this.directories = Lists.newArrayList();
        this.parentDirectory = parentDirectory;

        if (!dirFile.exists())
            dirFile.mkdir();
    }

    @SneakyThrows
    public Directory addFile(String name, String fileContent) {
        File file = new File(dirFile, name);
        if (!file.exists())
            file.createNewFile();
        if (fileContent != null && !fileContent.isEmpty()) {
            FileWriter writer = new FileWriter(file);
            writer.write(fileContent);
            writer.flush();
            writer.close();
        }
        return this;
    }

    public Directory addDirectoryAndDelete(String name) {
        File dir = new File(this.dirFile, name);
        if (dir.exists())
            FileUtils.deleteDirectoryAndContents(dir.toPath());
        return new Directory(dir.getPath(), this);
    }

    public Directory addDirectory(String name) {
        File dir = new File(this.dirFile, name);
        return new Directory(dir.getPath(), this);
    }

    public void indexFiles() {
        for (File listFile : this.dirFile.listFiles()) {
            if (listFile.isDirectory()) {
                if (getSubDirectory(listFile.getName()) == null) {
                    Directory directory = this.addDirectory(listFile.getName());
                    this.directories.add(directory);
                    directory.indexFiles();
                }
            } else {
                addFile(listFile.getName(), null);
            }
        }
    }

    public File getFile(String name) {
        for (File file : this.dirFile.listFiles()) {
            if (file.getName().equals(name)) return file;
        }
        return null;
    }

    public List<File> getFiles() {
        List<File> files = Lists.newArrayList();
        for (File file : this.dirFile.listFiles()) {
            if (!file.isDirectory())
                files.add(file);
        }

        return files;
    }

    public boolean containsFile(String name) {
        return getFile(name) != null;
    }

    public Directory getSubDirectory(String name) {
        return this.directories.stream().filter(directory -> directory.dirFile.getName().equals(name)).findAny().orElse(null);
    }

    @SneakyThrows
    public void delete() {
        this.parentDirectory.directories.remove(this);
        org.apache.commons.io.FileUtils.deleteDirectory(this.dirFile);
    }
}
