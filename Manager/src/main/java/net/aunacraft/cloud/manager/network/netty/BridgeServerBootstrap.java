package net.aunacraft.cloud.manager.network.netty;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.netty.handler.ServerChannelInitializer;

import java.util.concurrent.ThreadFactory;

public class BridgeServerBootstrap extends ServerBootstrap {

    public BridgeServerBootstrap(int port) {
        CloudManager.LOGGER.info("Starting netty server on port " + port);
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setDaemon(true).setNameFormat("ServerBridge-%d").build();
        EventLoopGroup eventLoopGroup = new EpollEventLoopGroup(threadFactory);
        this.group(eventLoopGroup);
        this.channel(EpollServerSocketChannel.class);
        this.childHandler(new ServerChannelInitializer());
        this.bind(port);
    }
}
