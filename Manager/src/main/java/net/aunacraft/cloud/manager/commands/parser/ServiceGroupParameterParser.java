package net.aunacraft.cloud.manager.commands.parser;

import de.febanhd.fcommand.executor.CommandExecutor;
import de.febanhd.fcommand.parser.ParameterParser;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;

public class ServiceGroupParameterParser implements ParameterParser<ServiceGroup> {

    @Override
    public boolean isValid(String in, CommandExecutor executor) {
        return CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroup(in) != null;
    }

    @Override
    public ServiceGroup convertString(String string) {
        return CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroup(string);
    }
}
