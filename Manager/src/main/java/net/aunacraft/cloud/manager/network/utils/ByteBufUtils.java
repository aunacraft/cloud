package net.aunacraft.cloud.manager.network.utils;

import com.google.common.collect.Lists;
import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.ServiceGroupType;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

public class ByteBufUtils {

    public static void writeString(ByteBuf byteBuf) {
        writeString(byteBuf, "UTF-8");
    }

    public static void writeString(ByteBuf byteBuf, String string) {
        byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
        byteBuf.writeInt(bytes.length);
        byteBuf.writeBytes(bytes);
    }

    public static String readString(ByteBuf byteBuf) {
        return readString(byteBuf, "UTF-8");
    }

    public static String readString(ByteBuf byteBuf, String charsetName) {
        int len = byteBuf.readInt();
        byte[] bytes = new byte[len];
        byteBuf.readBytes(bytes);
        String string = new String(bytes);
        return string;
    }

    public static void writeService(ByteBuf byteBuf, RunningService service) {
        byteBuf.writeInt(service.getPort());
        byteBuf.writeInt(service.getCurrentMemory());
        byteBuf.writeInt(service.getCurrentState().getId());
        writeString(byteBuf, service.getName());
        writeString(byteBuf, service.getGroup().getGroupName());
        writeString(byteBuf, service.getDisplayName());
        writeString(byteBuf, service.getScreenName());
        writeString(byteBuf, service.getTempFile().getAbsolutePath());
        byteBuf.writeInt(service.getMaxPlayers());
    }

    public static RunningService readService(ByteBuf byteBuf) {
        int port = byteBuf.readInt();
        int currentMemory = byteBuf.readInt();
        Service.ServiceState serviceState = Service.ServiceState.getByID(byteBuf.readInt());
        String name = readString(byteBuf, "UTF-8");
        String groupName = readString(byteBuf, "UTF-8");
        String displayName = readString(byteBuf, "UTF-8");
        String screenName = readString(byteBuf, "UTF-8");
        File tempDir = new File(readString(byteBuf, "UTF-8"));
        int maxPlayers = byteBuf.readInt();
        ServiceGroup group = CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroup(groupName);
        if (group == null) throw new NullPointerException("Cant find servicegroup with name: " + groupName);
        RunningService runningService = new RunningService(name, group, displayName, screenName, tempDir, serviceState, maxPlayers, port);
        runningService.setCurrentMemory(currentMemory);
        return runningService;
    }

    public static void writeServiceGroupList(ByteBuf byteBuf, List<ServiceGroup> groups) {
        byteBuf.writeInt(groups.size());
        for (ServiceGroup group : groups) {
            writeGroup(byteBuf, group);
        }
    }

    public static void writeGroup(ByteBuf byteBuf, ServiceGroup group) {
        writeString(byteBuf, group.getGroupName());
        writeString(byteBuf, group.getDisplayName());
        byteBuf.writeInt(group.getMaxPlayers());
        byteBuf.writeInt(group.getMaxMemory());
        byteBuf.writeInt(group.getMaxOnlineServices());
        byteBuf.writeInt(group.getMinOnlineServices());
        byteBuf.writeBoolean(group.isStatic());
        byteBuf.writeInt(group.getType().getId());
        byteBuf.writeInt(group.getPriority());
    }

    public static void writePlayer(ByteBuf byteBuf, NetworkPlayer player) {
        writeString(byteBuf, player.getName());
        writeString(byteBuf, player.getUuid().toString());
        writeString(byteBuf, player.getCurrentService().getName());
    }

    public static NetworkPlayer readPlayer(ByteBuf byteBuf) {
        String name = readString(byteBuf);
        UUID uuid = UUID.fromString(readString(byteBuf));
        String serviceName = readString(byteBuf);
        RunningService service = CloudManager.getInstance().getServiceManager().getGroupCache().getService(serviceName);
        return new NetworkPlayer(uuid, name, service);
    }

    public static void writePlayerList(ByteBuf byteBuf, List<NetworkPlayer> players) {
        byteBuf.writeInt(players.size());
        for (NetworkPlayer player : players) {
            writePlayer(byteBuf, player);
        }
    }

    public static List<NetworkPlayer> readPlayerList(ByteBuf byteBuf) {
        List<NetworkPlayer> players = Lists.newArrayList();
        int size = byteBuf.readInt();
        for (int i = 0; i < size; i++) {
            players.add(readPlayer(byteBuf));
        }
        return players;
    }
}
