package net.aunacraft.cloud.manager.file;

import lombok.SneakyThrows;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.utils.IOUtils;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileDownloader {

    private final URL URL;
    private final String FILE_NAME;

    @SneakyThrows
    public FileDownloader(String url, String fileName) {
        this.URL = new URL(url);
        this.FILE_NAME = fileName;
    }

    public void downloadAndPaste(File pasteDir) {
        CloudManager.LOGGER.info("Downloading " + FILE_NAME + " from " + URL);
        ReadableByteChannel readableByteChannel = null;
        FileOutputStream fileOutputStream = null;
        File file = null;
        try {
            readableByteChannel = Channels.newChannel(URL.openStream());
            fileOutputStream = new FileOutputStream(pasteDir.getAbsoluteFile().toString() + "/" + FILE_NAME);
            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
            file = new File(pasteDir.getAbsoluteFile(), FILE_NAME);
        } catch (IOException e) {
            CloudManager.LOGGER.error("Cant download file", e);
        } finally {
            IOUtils.closeQuickly(readableByteChannel);
            IOUtils.closeQuickly(fileOutputStream);
        }
    }

    public void downloadFile(File pasteDir) {
        CloudManager.LOGGER.info("Downloading " + FILE_NAME + " from " + URL);
        try {
            FileUtils.copyURLToFile(URL, new File(pasteDir, FILE_NAME));
        }catch (Exception e) {
            CloudManager.LOGGER.error("Cant download file", e);
        }
    }
}
