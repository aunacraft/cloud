package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

@Getter
public class PacketInStopService implements Packet {

    private String serviceName;

    public PacketInStopService(String serviceName) {
        this.serviceName = serviceName;
    }

    public PacketInStopService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.serviceName = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(PacketListener packetListener) {
        packetListener.handleStopServicePacket(this);
    }
}
