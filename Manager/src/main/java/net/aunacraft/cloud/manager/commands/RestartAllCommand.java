package net.aunacraft.cloud.manager.commands;

import de.febanhd.fcommand.FCommand;
import de.febanhd.fcommand.builder.CommandBuilder;
import de.febanhd.fcommand.builder.ParameterBuilder;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.commands.parser.ServiceGroupParameterParser;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;

public class RestartAllCommand implements FCommand {

    @Override
    public CommandBuilder create(CommandBuilder builder) {
        return builder
                .handler((executor, ctx) -> {
                    for (ServiceGroup group : CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroups()) {
                        int services = group.getOnlineServices().size();
                        for (RunningService onlineService : group.getOnlineServices()) {
                            onlineService.stop();
                        }
                        for (int i = 0; i < services; i++) {
                            group.startNewService();
                        }
                    }
                });
    }
}
