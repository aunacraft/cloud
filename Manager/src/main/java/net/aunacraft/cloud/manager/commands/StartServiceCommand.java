package net.aunacraft.cloud.manager.commands;

import de.febanhd.fcommand.FCommand;
import de.febanhd.fcommand.builder.CommandBuilder;
import de.febanhd.fcommand.builder.ParameterBuilder;
import net.aunacraft.cloud.manager.commands.parser.ServiceGroupParameterParser;
import net.aunacraft.cloud.manager.commands.parser.ServiceParameterParser;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;

public class StartServiceCommand implements FCommand {

    @Override
    public CommandBuilder create(CommandBuilder builder) {
        return builder
                .parameter(
                        ParameterBuilder.beginParameter("group")
                                .required()
                                .parser(new ServiceGroupParameterParser())
                                .build()
                )
                .handler((executor, ctx) -> {
                    ServiceGroup group = ctx.getParameterValue("group", ServiceGroup.class);
                    group.startNewService();
                })
                ;
    }
}
