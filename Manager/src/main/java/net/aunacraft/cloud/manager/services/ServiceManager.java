package net.aunacraft.cloud.manager.services;

import lombok.Getter;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutAddServiceGroup;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutStopService;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.impl.SpigotServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;
import net.aunacraft.cloud.manager.services.utils.StartingQueue;

import java.util.Arrays;
import java.util.List;

@Getter
public class ServiceManager {

    private final StartingQueue startingQueue;
    private final ServiceGroupCache groupCache;

    public ServiceManager() {
        this.groupCache = new ServiceGroupCache();
        this.startingQueue = new StartingQueue();
    }

    public void tick() {
        this.startingQueue.tick();
        groupCache.getServiceGroups().forEach(group -> {
            group.tick();
            if(group.needNewService(startingQueue)) {
                this.startingQueue.add(group);
            }
            for (RunningService onlineService : group.getOnlineServices()) {
                onlineService.tick();
            }
        });
    }

    public void sendPacketToAllServices(Packet packet) {
        for (ServiceGroup serviceGroup : this.groupCache.getServiceGroups()) {
            for (RunningService onlineService : serviceGroup.getOnlineServices()) {
                if(onlineService.isRunning()) {
                    onlineService.getNetworkManager().sendPacket(packet);
                }
            }
        }
    }

    public void createServiceGroup(String groupName, String displayName, boolean isStatic, int minOnlineServices, int maxOnlineServices, int maxPlayers, int maxMemory, int priority) {
        SpigotServiceGroup serviceGroup = new SpigotServiceGroup(groupName, displayName, isStatic, maxOnlineServices, minOnlineServices, maxPlayers, maxMemory, priority);
        this.groupCache.addSpigotGroup(serviceGroup);
        this.sendPacketToAllServices(new PacketOutAddServiceGroup(List.of(serviceGroup)));
    }

    public void stopAllServices() {
        this.groupCache.getServiceGroups().forEach(group -> group.getOnlineServices().forEach(runningService -> {
            if(runningService.getCurrentState() == Service.ServiceState.STARTING) {
                CloudManager.LOGGER.info("Killing starting service '" + runningService.getName() + "'");
                runningService.killScreen();
            }else {
                runningService.stop();
            }
        }));
    }

    public boolean canStartService(int memory) {
        int usedMemory = 0;
        for (ServiceGroup serviceGroup : this.getGroupCache().getServiceGroups()) {
            usedMemory += serviceGroup.getMaxMemory() * serviceGroup.getOnlineServices().size();
        }
        if(usedMemory + memory > CloudManager.getInstance().getFileSystem().getConfigDirectory().getDefaultConfig().getMaxMemory()) return false;
        return true;
    }

}
