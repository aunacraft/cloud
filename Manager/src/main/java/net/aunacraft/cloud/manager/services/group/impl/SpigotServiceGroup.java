package net.aunacraft.cloud.manager.services.group.impl;

import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.file.Directory;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutStartingSpigotService;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.ServiceGroupType;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;
import net.aunacraft.cloud.manager.utils.IOUtils;
import net.aunacraft.cloud.manager.utils.SHUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SpigotServiceGroup extends ServiceGroup {

    public SpigotServiceGroup(String groupName, String displayName, boolean isStatic, int maxOnlineServices, int minOnlineServices, int maxPlayers, int maxMemory, int priority) {
        super(groupName, displayName, isStatic, maxOnlineServices, minOnlineServices, maxPlayers,
                CloudManager.getInstance().getFileSystem().createGroupDirectory(groupName, isStatic), maxMemory, ServiceGroupType.SPIGOT, priority);
    }

    @Override
    public RunningService createService(String name, int id, String identifier, int port, File jarFile, File workingDir) throws IOException {
        File eulaFile = new File(workingDir, "eula.txt");
        if (eulaFile.exists())
            eulaFile.delete();
        eulaFile.createNewFile();
        FileWriter eulaFileWriter = new FileWriter(eulaFile);
        eulaFileWriter.write("eula=true");
        eulaFileWriter.flush();
        IOUtils.closeQuickly(eulaFileWriter);

        File propertiesFile = new File(workingDir, "server.properties");
        if (propertiesFile.exists())
            propertiesFile.delete();
        propertiesFile.createNewFile();
        FileWriter propertiesFileWriter = new FileWriter(propertiesFile);
        propertiesFileWriter.write("#Minecraft server properties\n" +
                "#Mon Mar 15 11:34:24 CET 2021\n" +
                "spawn-protection=0\n" +
                "max-tick-time=60000\n" +
                "generator-settings=\n" +
                "sync-chunk-writes=true\n" +
                "force-gamemode=false\n" +
                "allow-nether=true\n" +
                "enforce-whitelist=false\n" +
                "gamemode=survival\n" +
                "broadcast-console-to-ops=true\n" +
                "enable-query=false\n" +
                "player-idle-timeout=0\n" +
                "text-filtering-config=\n" +
                "difficulty=easy\n" +
                "broadcast-rcon-to-ops=true\n" +
                "spawn-monsters=true\n" +
                "op-permission-level=0\n" +
                "pvp=true\n" +
                "entity-broadcast-range-percentage=100\n" +
                "snooper-enabled=true\n" +
                "level-type=default\n" +
                "enable-status=true\n" +
                "hardcore=false\n" +
                "enable-command-block=false\n" +
                "network-compression-threshold=256\n" +
                "max-world-size=29999984\n" +
                "resource-pack-sha1=\n" +
                "function-permission-level=2\n" +
                "debug=false\n" +
                "spawn-npcs=true\n" +
                "allow-flight=false\n" +
                "level-name=world\n" +
                "view-distance=10\n" +
                "resource-pack=\n" +
                "spawn-animals=true\n" +
                "white-list=false\n" +
                "rcon.password=\n" +
                "generate-structures=true\n" +
                "online-mode=false\n" +
                "max-build-height=256\n" +
                "level-seed=\n" +
                "prevent-proxy-connections=false\n" +
                "use-native-transport=true\n" +
                "enable-jmx-monitoring=false\n" +
                "motd=" + name + "\n" +
                "rate-limit=0\n" +
                "enable-rcon=false\n");
        propertiesFileWriter.flush();
        IOUtils.closeQuickly(propertiesFileWriter);

        String screenName = name + "_" + identifier;
        SHUtils.executeCommand(
                "screen -mdS " + screenName +
                        " /bin/sh -c '" +
                        "cd " + workingDir.getAbsolutePath() + " &&" +
                        " java" +
                        " -Xms" + getMaxMemory() + "M" +
                        " -Xmx" + this.getMaxMemory() + "M" +
                        " -XX:+UseG1GC" +
                        " -XX:+UnlockExperimentalVMOptions" +
                        " -XX:+ParallelRefProcEnabled" +
                        " -XX:+AlwaysPreTouch" +
                        " -XX:G1HeapRegionSize=8M" +
                        " -XX:G1ReservePercent=20" +
                        " -XX:G1HeapWastePercent=5" +
                        " -XX:G1MixedGCCountTarget=4" +
                        " -XX:MaxGCPauseMillis=50" +
                        " -XX:+DisableExplicitGC" +
                        " -XX:TargetSurvivorRatio=90" +
                        " -XX:G1NewSizePercent=30" +
                        " -XX:G1MaxNewSizePercent=40" +
                        " -XX:InitiatingHeapOccupancyPercent=15" +
                        " -XX:G1MixedGCLiveThresholdPercent=90" +
                        " -XX:G1RSetUpdatingPauseTimePercent=5" +
                        " -XX:SurvivorRatio=32" +
                        " -XX:+PerfDisableSharedMem" +
                        " -XX:MaxTenuringThreshold=1" +
                        " -jar " + jarFile.getName() +
                        " -p" + port +
                        " -s" + 10000 +
                        " -nogui" +
                        "'"
        );

        String displayName = this.getDisplayName().replace("%d", String.valueOf(id));
        CloudManager.getInstance().getServiceManager().sendPacketToAllServices(new PacketOutStartingSpigotService(name));
        return new RunningService(name, this, displayName, screenName, workingDir, Service.ServiceState.STARTING, this.getMaxPlayers(), port);
    }
}
