package net.aunacraft.cloud.manager.utils;

import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class FileUtils {

    @SneakyThrows
    public static void copyFolder(Path src, Path dest, StandardCopyOption copyOption) {
        Files.walk(src).forEach(path -> copyFile(path, dest.resolve(src.relativize(path)), copyOption));
    }

    private static void copyFile(Path src, Path dest, StandardCopyOption copyOption) {
        try {
            if (!Files.exists(dest)) {
                Files.copy(src, dest, copyOption);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SneakyThrows
    public static void deleteDirectoryAndContents(Path path) {
        Files.walk(path).sorted().map(obj -> obj.toFile()).forEach(obj -> obj.delete());
    }
}
