package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.service.RunningService;

import java.util.UUID;

public class PacketInPlayerSwitchService implements Packet {

    private UUID uuid;
    private String serviceName;

    public PacketInPlayerSwitchService(UUID uuid, String serviceName) {
        this.uuid = uuid;
        this.serviceName = serviceName;
    }

    public PacketInPlayerSwitchService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, uuid.toString());
        ByteBufUtils.writeString(byteBuf, this.serviceName);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = UUID.fromString(ByteBufUtils.readString(byteBuf));
        this.serviceName = ByteBufUtils.readString(byteBuf);
    }

    @Override
    public void handle(PacketListener packetListener) {
        RunningService service = CloudManager.getInstance().getServiceManager().getGroupCache().getService(this.serviceName);
        NetworkPlayer player = PlayerCache.getPlayer(uuid);
        if(player == null) return;
        player.setCurrentService(service);
    }
}
