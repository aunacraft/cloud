package net.aunacraft.cloud.manager.commands.parser;

import de.febanhd.fcommand.executor.CommandExecutor;
import de.febanhd.fcommand.parser.ParameterParser;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.services.service.RunningService;

public class ServiceParameterParser implements ParameterParser<RunningService> {

    @Override
    public boolean isValid(String in, CommandExecutor executor) {
        RunningService service = CloudManager.getInstance().getServiceManager().getGroupCache().getService(in);
        return service != null && service.isRunning();
    }

    @Override
    public RunningService convertString(String string) {
        return CloudManager.getInstance().getServiceManager().getGroupCache().getService(string);
    }
}
