package net.aunacraft.cloud.manager.network.manager;

import com.google.common.collect.Lists;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.unix.Errors;
import io.netty.handler.timeout.ReadTimeoutException;
import lombok.Getter;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.packets.out.*;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.impl.ProxyServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;

import java.util.concurrent.CopyOnWriteArrayList;

@Getter
public class NetworkManager extends SimpleChannelInboundHandler<Packet> {

    private RunningService service;
    private final PacketListener packetListener = new PacketListener(this);
    private Channel channel;
    private boolean registered;
    private final CopyOnWriteArrayList<Packet> queue = Lists.newCopyOnWriteArrayList();
    private long lastKeepAlive = 0;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.channel = ctx.channel();
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        if(registered) {
            stop();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if(cause instanceof ReadTimeoutException) {
            stop();
            //TODO: make a schedulerr system
            CloudManager.EXECUTOR_SERVICE.execute(() -> {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.service.handleTimeOut();
            });
        }else {
            if(!(cause instanceof Errors.NativeIoException)) {
                cause.printStackTrace();
            }
            stop();
        }
    }

    public void close() {
        this.channel.close();
    }

    public void stop() {
        if(channel.isActive()) {
            CloudManager.LOGGER.info("Unregister service '" + this.service.getName() + "'");
            this.sendPacket(new PacketOutStopService());
        }
        this.service.setCurrentState(Service.ServiceState.STOPPING);
        this.service.getGroup().getOnlineServices().remove(service);
        this.sendPacketToServices(new PacketOutUnregisterService(service.getName()));
        this.close();

        if(this.service.getGroup() instanceof ProxyServiceGroup proxyServiceGroup) {
            proxyServiceGroup.removeAllPlayers();
        }
    }

    public void setService(RunningService service) {
        this.service = service;
        this.registered = true;
        service.setNetworkManager(this);
        sendPacket(new PacketOutAddServiceGroup(CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroups()));
        sendPacket(new PacketOutRegisterService(this.service,
                CloudManager.getInstance().getServiceManager().getGroupCache().getProxyServiceGroup().getService(),
                CloudManager.getInstance().getServiceManager().getGroupCache().getFallbackGroup().getGroupName()

        ));
        for (ServiceGroup serviceGroup : CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroups()) {
            for (RunningService onlineService : serviceGroup.getOnlineServices()) {
                if(onlineService.isRunning() && onlineService != this.service) {
                    sendPacket(new PacketOutAddSpigotService(onlineService));
                }
            }
        }

        sendPacketToServices(new PacketOutAddSpigotService(service));

        sendPacket(new PacketOutRegisterPlayers(PlayerCache.getOnlinePlayers()));

        CloudManager.LOGGER.info("Registered service '" + service.getName() + "'");
    }

    public void sendPacket(Packet packet) {
        synchronized (queue) {
            queue.add(packet);
        }
    }

    public void sendPacketDirect(Packet packet) {
        this.channel.writeAndFlush(packet, this.channel.voidPromise());
    }

    /*
    * Send the packet to all other services
    */

    public void sendPacketToServices(Packet packet) {
        CloudManager.getInstance().getServiceManager().getGroupCache().getServiceGroups()
                .forEach(group -> {
                    group.sendPacketToOnlineServices(this.service, packet);
                });
    }

    public void tick() {
        synchronized (queue) {
            if(!queue.isEmpty()) {
                for (Packet packet : queue) {
                    this.channel.write(packet, this.channel.voidPromise());
                }
                channel.flush();
                queue.clear();
            }
        }
        if(lastKeepAlive + 5000 <= System.currentTimeMillis()) {
            this.sendPacket(new PacketOutKeepAlive());
            this.lastKeepAlive = System.currentTimeMillis();
        }

    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet packet) throws Exception {
        packet.handle(packetListener);
    }
}
