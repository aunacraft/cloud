package net.aunacraft.cloud.manager.network.manager;

import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.packets.in.*;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutPluginMessage;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutStopService;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutUpdateService;
import net.aunacraft.cloud.manager.services.ServiceGroupCache;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.impl.ProxyServiceGroup;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;

public class PacketListener {

    private final NetworkManager networkManager;
    private final ServiceGroupCache serviceGroupCache = CloudManager.getInstance().getServiceManager().getGroupCache();

    public PacketListener(NetworkManager networkManager) {
        this.networkManager = networkManager;
    }

    public void handleRegisterPacket(PacketInRegisterService packet) {
        int servicePort = packet.getPort();
        Service service = serviceGroupCache.getStartingService(servicePort);
        if(service == null)
            this.networkManager.close();
        if(service instanceof RunningService runningService)
            networkManager.setService(runningService);
    }

    public void handleStopServicePacket(PacketInStopService packet) {
        RunningService service = serviceGroupCache.getService(packet.getServiceName());
        if(service == null) {
            CloudManager.LOGGER.debug("service is null");
            return;
        }
        if(service.getGroup() instanceof ProxyServiceGroup) {
            service.getNetworkManager().sendPacket(new PacketOutStopService());
        }else
            service.stop();
    }

    public void handleStartServicePacket(PacketInStartSpigotService packet) {
        ServiceGroup group = serviceGroupCache.getServiceGroup(packet.getGroupName());
        if(group == null) {
            CloudManager.LOGGER.debug("group is null");
            return;
        }
        group.startNewService();
    }

    public void handleUpdateServicePacket(PacketInUpdateService packet) {
        RunningService service = networkManager.getService();
        RunningService updatedService = packet.getUpdatedService();
        service.handleUpdatePacket(updatedService);
        service.sendUpdate();
    }

    public void handlePluginMessage(PacketInPluginMessage packet) {
        String serviceName = packet.getServiceName();
        RunningService service = CloudManager.getInstance().getServiceManager().getGroupCache().getService(serviceName);
        if(service == null) {
            CloudManager.LOGGER.warn("Cant handle Plugin-Message... Receiver doesn't exists");
            return;
        }
        service.getNetworkManager().sendPacketDirect(new PacketOutPluginMessage(packet));
    }

}
