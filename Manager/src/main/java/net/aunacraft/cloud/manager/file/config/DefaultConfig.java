package net.aunacraft.cloud.manager.file.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import net.aunacraft.cloud.manager.file.FileSystem;

@Getter
public class DefaultConfig {

    private final String fallbackGroup;
    private final String bridgeServerURL;
    private final int maxPlayers;
    private final int bungeeCordMemory;
    private final int maxMemory;

    public DefaultConfig() {
        this.fallbackGroup = "Lobby";
        this.bridgeServerURL = "http://dev.aunacraft.net/backend/";
        this.maxPlayers = 50;
        this.bungeeCordMemory = 1024;
        this.maxMemory = 10000;
    }

    public String toJson() {
        return FileSystem.GSON.toJson(this);
    }

    public static DefaultConfig fromJson(String json) {
        return FileSystem.GSON.fromJson(json, DefaultConfig.class);
    }
}
