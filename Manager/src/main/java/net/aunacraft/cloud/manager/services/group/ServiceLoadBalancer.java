package net.aunacraft.cloud.manager.services.group;

import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.services.service.RunningService;
import net.aunacraft.cloud.manager.services.service.Service;
import net.aunacraft.cloud.manager.services.utils.StartingQueue;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public record ServiceLoadBalancer(ServiceGroup group) {

    public boolean needNewService(StartingQueue startingQueue) {
        if(!needToStartNewService(startingQueue))
            return false;


        //check if there is a service in wating for stop mode
        for (RunningService onlineService : group.getOnlineServices()) {
            if(onlineService.getCurrentState() == Service.ServiceState.WAIT_FOR_STOP) {
                onlineService.setCurrentState(Service.ServiceState.RUNNING);
                return false;
            }
        }

        return true;

    }

    private boolean needToStartNewService(StartingQueue startingQueue) {
        int queuedAmount = startingQueue.getServiceAmountInQueue(group);
        int onlineAmount = (int) group.getOnlineServices().stream().filter(runningService -> runningService.getCurrentState() != Service.ServiceState.WAIT_FOR_STOP).count();

        //service amount < minimum
        if (onlineAmount + queuedAmount < group.getMinOnlineServices()) return true;

        //service amount >= maximum
        if (group.getMinOnlineServices() >= 0 && onlineAmount + queuedAmount >= group.getMaxOnlineServices())
            return false;

        //already starting a service
        if (queuedAmount > 0) return false;

        //amount of services who are < 70% in use
        int freeOnlineServices = 0;
        for (Service onlineService : group.getOnlineServices()) {
            if (onlineService.getCurrentState() == Service.ServiceState.WAIT_FOR_STOP)
                continue;
            if (onlineService.getOnlinePlayers().size() < group.getMaxOnlineServices() * 0.7)
                freeOnlineServices++;
        }
        if (freeOnlineServices == 0) return true;

        return false;
    }

    public void tick() {

        this.group.getOnlineServices().stream()
                .filter(service -> service.getCurrentState() == Service.ServiceState.WAIT_FOR_STOP && service.getOnlinePlayers().size() == 0)
                .forEach(RunningService::stop);

        int onlineAmount = (int) group.getOnlineServices().stream().filter(runningService -> runningService.getCurrentState() != Service.ServiceState.WAIT_FOR_STOP).count();
        if (onlineAmount - 1 < group.getMinOnlineServices()) return;


        int groupMaxOnlinePlayers =
                (
                        (int) this.group.getOnlineServices().stream().filter(service -> service.getCurrentState() == Service.ServiceState.RUNNING)
                                .count() + CloudManager.getInstance().getServiceManager().getStartingQueue().getServiceAmountInQueue(group)
                )
                        * this.group.getMaxPlayers();
        AtomicInteger groupOnlinePlayerAmount = new AtomicInteger();
        group.getOnlineServices().stream().filter(service -> service.getCurrentState() == Service.ServiceState.RUNNING)
                .forEach(service -> groupOnlinePlayerAmount.addAndGet(service.getOnlinePlayers().size()));


        RunningService serviceWithLowestPlayerAmount = null;

        for (RunningService onlineService : group.getOnlineServices()) {
            if(onlineService.getCurrentState() != Service.ServiceState.RUNNING)
                continue;
            if (serviceWithLowestPlayerAmount == null || onlineService.getOnlinePlayers().size() < serviceWithLowestPlayerAmount.getOnlinePlayers().size())
                serviceWithLowestPlayerAmount = onlineService;
        }

        if (serviceWithLowestPlayerAmount == null || serviceWithLowestPlayerAmount.getStartedAt() + TimeUnit.MINUTES.toMillis(5) > System.currentTimeMillis()) return;

        if ((groupOnlinePlayerAmount.get() - serviceWithLowestPlayerAmount.getOnlinePlayers().size()) < 0.7 * (groupMaxOnlinePlayers - group.getMaxPlayers())) {
            serviceWithLowestPlayerAmount.setCurrentState(Service.ServiceState.WAIT_FOR_STOP);
        }
    }

}
