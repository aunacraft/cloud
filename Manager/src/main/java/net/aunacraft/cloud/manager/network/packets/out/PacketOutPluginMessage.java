package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.packets.in.PacketInPluginMessage;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;

@Getter
public class PacketOutPluginMessage implements Packet {

    private String properties;
    private String type;

    public PacketOutPluginMessage(String properties, String type) {
        this.properties = properties;
        this.type = type;
    }

    public PacketOutPluginMessage(PacketInPluginMessage packet) {
        this(packet.getProperties(), packet.getType());
    }

    public PacketOutPluginMessage() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.type);
        ByteBufUtils.writeString(byteBuf, this.properties);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.type = ByteBufUtils.readString(byteBuf, "UTF-8");
        this.properties = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(PacketListener packetListener) {

    }
}
