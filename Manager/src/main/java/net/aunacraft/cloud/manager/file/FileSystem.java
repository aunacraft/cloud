package net.aunacraft.cloud.manager.file;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.file.config.ConfigDirectory;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.services.group.ServiceGroupType;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Files;

@Getter
public class FileSystem {

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    private Directory runDirectory = new Directory("./", null),
            tempDirectory = runDirectory.addDirectoryAndDelete("temp"),
            jarsDirectory = runDirectory.addDirectory("jars"),
            groupDirectory = runDirectory.addDirectory("groups");
    private ConfigDirectory configDirectory;

    @SneakyThrows
    public FileSystem() {
        CloudManager.LOGGER.info("Creating files");
        groupDirectory.addDirectory("all").addDirectory("spigot").addDirectory("plugins");
        groupDirectory.addDirectory("templates");
        groupDirectory.addDirectory("static");
        this.configDirectory = new ConfigDirectory("config", runDirectory);

        for (ServiceGroupType type : ServiceGroupType.values()) {
            if (!jarsDirectory.containsFile(type.getDefaultJarName())) {
                CloudManager.LOGGER.warn(type.getDefaultJarName() + " is missing in " + jarsDirectory.getDirFile().getAbsolutePath());
                new FileDownloader(type.getDownloadURL(), type.getDefaultJarName())
                        .downloadFile(jarsDirectory.getDirFile());
            }
        }

        CloudManager.LOGGER.info("downloading Bridge Plugins...");
        if (jarsDirectory.getFile("ProxyBridge.jar") != null)
            jarsDirectory.getFile("ProxyBridge.jar").delete();
        if (jarsDirectory.getFile("SpigotBridge.jar") != null)
            jarsDirectory.getFile("SpigotBridge.jar").delete();

        new FileDownloader(configDirectory.getDefaultConfig().getBridgeServerURL() + "cloud/ProxyBridge.jar", "ProxyBridge.jar")
                .downloadFile(jarsDirectory.getDirFile());
        new FileDownloader(configDirectory.getDefaultConfig().getBridgeServerURL() + "cloud/SpigotBridge.jar", "SpigotBridge.jar")
                .downloadFile(jarsDirectory.getDirFile());

        CloudManager.LOGGER.info("Indexing files");
        runDirectory.indexFiles();

        CloudManager.LOGGER.info("Delete old temp files");
        FileUtils.deleteDirectory(tempDirectory.getDirFile());
        tempDirectory.getDirFile().mkdir();
    }

    public Directory createGroupDirectory(String name, boolean isStatic) {
        String dirName = isStatic ? "static" : "templates";
        Directory directory = runDirectory.addDirectory("groups").addDirectory(dirName).addDirectory(name);
        directory.addDirectory("plugins");
        directory.indexFiles();
        return directory;
    }

    public Directory getSpigotAllDir() {
        return groupDirectory.addDirectory("all").addDirectory("spigot");
    }

    @SneakyThrows
    public void copyCloudBridgePlugin(File pluginDir, ServiceGroupType type) {
        File pluginFile = type == ServiceGroupType.SPIGOT ? jarsDirectory.getFile("SpigotBridge.jar") : jarsDirectory.getFile("ProxyBridge.jar");
        File aunaPluginFile = new File(pluginDir, "AunaCloudAPI.jar");
        if (aunaPluginFile.exists())
            aunaPluginFile.delete();
        Files.copy(pluginFile.toPath(), aunaPluginFile.toPath());
    }

}
