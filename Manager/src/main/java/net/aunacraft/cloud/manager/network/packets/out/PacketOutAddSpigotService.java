package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.service.RunningService;

public class PacketOutAddSpigotService implements Packet {

    private RunningService service;

    public PacketOutAddSpigotService(RunningService service) {
        this.service = service;
    }

    public PacketOutAddSpigotService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeService(byteBuf, service);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.service = ByteBufUtils.readService(byteBuf);
    }

    @Override
    public void handle(PacketListener packetListener) {

    }
}
