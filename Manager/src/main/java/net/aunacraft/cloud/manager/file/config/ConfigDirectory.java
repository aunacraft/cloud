package net.aunacraft.cloud.manager.file.config;

import lombok.Getter;
import lombok.SneakyThrows;
import net.aunacraft.cloud.manager.file.Directory;

import java.io.File;
import java.nio.file.Files;

@Getter
public class ConfigDirectory extends Directory {

    private final DefaultConfig defaultConfig;

    @SneakyThrows
    public ConfigDirectory(String path, Directory parentDirectory) {
        super(path, parentDirectory);
        File file = getFile("config.json");
        System.out.println(file);
        if (file != null && file.exists()) {
            defaultConfig = DefaultConfig.fromJson(new String(Files.readAllBytes(file.toPath())));
        } else {
            defaultConfig = new DefaultConfig();
            addFile("config.json", defaultConfig.toJson());
        }
    }
}
