package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;
import net.aunacraft.cloud.manager.network.utils.ByteBufUtils;
import net.aunacraft.cloud.manager.services.service.RunningService;

@Getter
public class PacketInPluginMessage implements Packet {

    private String serviceName;
    private String properties;
    private String type;

    public PacketInPluginMessage(String serviceName, String properties, String type) {
        this.serviceName = serviceName;
        this.properties = properties;
        this.type = type;
    }

    public PacketInPluginMessage() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        ByteBufUtils.writeString(byteBuf, this.serviceName);
        ByteBufUtils.writeString(byteBuf, this.type);
        ByteBufUtils.writeString(byteBuf, this.properties);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.serviceName = ByteBufUtils.readString(byteBuf, "UTF-8");
        this.type = ByteBufUtils.readString(byteBuf, "UTF-8");
        this.properties = ByteBufUtils.readString(byteBuf, "UTF-8");
    }

    @Override
    public void handle(PacketListener packetListener) {
        packetListener.handlePluginMessage(this);
    }
}
