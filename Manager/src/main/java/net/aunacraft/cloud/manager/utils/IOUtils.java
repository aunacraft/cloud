package net.aunacraft.cloud.manager.utils;

import net.aunacraft.cloud.manager.CloudManager;

import java.io.Closeable;
import java.io.IOException;

public class IOUtils {

    public static void closeQuickly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                CloudManager.LOGGER.error("Cant close closeable", e);
            }
        }
    }
}
