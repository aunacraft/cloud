package net.aunacraft.cloud.manager.network.packets.in;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import net.aunacraft.cloud.manager.network.manager.NetworkManager;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;

@Getter
public class PacketInRegisterService implements Packet {

    private int port;

    public PacketInRegisterService(int port) {
        this.port = port;
    }

    public PacketInRegisterService() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        byteBuf.writeInt(this.port);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.port = byteBuf.readInt();
    }

    @Override
    public void handle(PacketListener listener) {
        listener.handleRegisterPacket(this);
    }
}
