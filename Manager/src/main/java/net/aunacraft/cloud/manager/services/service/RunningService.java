package net.aunacraft.cloud.manager.services.service;

import com.google.common.collect.Lists;
import lombok.Getter;
import net.aunacraft.cloud.manager.CloudManager;
import net.aunacraft.cloud.manager.network.manager.NetworkManager;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutStopService;
import net.aunacraft.cloud.manager.network.packets.out.PacketOutUpdateService;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.network.player.PlayerCache;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;
import net.aunacraft.cloud.manager.utils.SHUtils;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Getter
public class RunningService implements Service {

    private NetworkManager networkManager;
    private final String name;
    private final ServiceGroup group;
    private final String displayName;
    private int maxPlayers;
    private final String screenName;
    private final File tempFile;
    private int currentMemory;
    private final int port;
    private ServiceState currentState;
    private final long startedAt = System.currentTimeMillis();

    public RunningService(String name, ServiceGroup group, String displayName, String screenName, File tempFile, ServiceState currentState, int maxPlayers, int port) {
        this.name = name;
        this.group = group;
        this.displayName = displayName;
        this.screenName = screenName;
        this.tempFile = tempFile;
        this.currentState = currentState;
        this.currentMemory = 0;
        this.port = port;
        this.maxPlayers = maxPlayers;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public ServiceGroup getGroup() {
        return group;
    }

    @Override
    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    @Override
    public String getScreenName() {
        return this.screenName;
    }

    @Override
    public int getCurrentMemory() {
        return this.currentMemory;
    }

    @Override
    public List<NetworkPlayer> getOnlinePlayers() {
        return PlayerCache.getPlayersOnService(this);
    }

    @Override
    public NetworkManager getNetworkManager() {
        return this.networkManager;
    }

    @Override
    public ServiceState getCurrentState() {
        return this.currentState;
    }

    @Override
    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        sendUpdate();
    }

    public File getTempFile() {
        return this.tempFile;
    }

    @Override
    public int getPort() {
        return this.port;
    }

    public void setNetworkManager(NetworkManager networkManager) {
        this.networkManager = networkManager;
    }

    public void setCurrentState(ServiceState currentState) {
        this.currentState = currentState;
        sendUpdate();
    }

    public void setCurrentMemory(int currentMemory) {
        this.currentMemory = currentMemory;
    }

    public void setCurrentMemoryAndUpdate(int currentMemory) {
        setCurrentMemory(currentMemory);
        sendUpdate();
    }

    public boolean isRunning() {
        return this.currentState == ServiceState.RUNNING && this.networkManager != null;
    }

    public void stop() {
        if(isRunning()) {
            this.networkManager.stop();
        }
    }

    public void handleUpdatePacket(RunningService updatedService) {
        this.currentMemory = updatedService.getCurrentMemory();
        this.currentState = updatedService.getCurrentState();
        this.maxPlayers = updatedService.getMaxPlayers();
    }

    public void sendUpdate() {
        if(this.networkManager != null) {
            PacketOutUpdateService packet = new PacketOutUpdateService(this);
            this.networkManager.sendPacketToServices(packet);
            this.networkManager.sendPacket(packet);
        }
    }

    public void tick() {
        if(this.networkManager != null)
            this.networkManager.tick();
        if(this.startedAt + TimeUnit.MINUTES.toMillis(5) < System.currentTimeMillis() && !isRunning()) {
            this.handleTimeOut();
        }
    }

    public synchronized void handleTimeOut() {
        CloudManager.LOGGER.info("Service '" + this.getName() + "' timeout. Killing screen...");
        this.group.removeService(this);
        killScreen();
    }

    public void killScreen() {
        SHUtils.executeCommand("screen -X -S " + this.screenName + " kill");
    }
}
