package net.aunacraft.cloud.manager.utils;

import lombok.SneakyThrows;

public class SHUtils {

    @SneakyThrows
    public static void executeCommand(String command) {
        new ProcessBuilder("/bin/sh", "-c", command).start();
    }
}
