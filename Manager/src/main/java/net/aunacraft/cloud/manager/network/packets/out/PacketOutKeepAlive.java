package net.aunacraft.cloud.manager.network.packets.out;

import io.netty.buffer.ByteBuf;
import net.aunacraft.cloud.manager.network.manager.PacketListener;
import net.aunacraft.cloud.manager.network.packets.Packet;

public class PacketOutKeepAlive implements Packet {

    private int id;

    public PacketOutKeepAlive(int id) {
        this.id = id;
    }

    public PacketOutKeepAlive() {
    }

    @Override
    public void write(ByteBuf byteBuf) {
        byteBuf.writeInt(this.id);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.id = byteBuf.readInt();
    }

    @Override
    public void handle(PacketListener networkManager) {
        
    }
}
