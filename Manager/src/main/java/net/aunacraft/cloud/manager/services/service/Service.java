package net.aunacraft.cloud.manager.services.service;

import lombok.Getter;
import net.aunacraft.cloud.manager.network.manager.NetworkManager;
import net.aunacraft.cloud.manager.network.player.NetworkPlayer;
import net.aunacraft.cloud.manager.services.group.ServiceGroup;

import java.util.List;

public interface Service {

    String getName();

    ServiceGroup getGroup();

    int getMaxPlayers();

    String getDisplayName();

    String getScreenName();

    int getCurrentMemory();

    List<NetworkPlayer> getOnlinePlayers();

    NetworkManager getNetworkManager();

    ServiceState getCurrentState();

    int getPort();

    void setMaxPlayers(int maxPlayers);

    @Getter
    enum ServiceState {

        STARTING(0), RUNNING(1), STOPPING(2), WAIT_FOR_STOP(3);

        private final int id;

        ServiceState(int id) {
            this.id = id;
        }

        public static ServiceState getByID(int id) {
            for (ServiceState value : values()) {
                if(value.getId() == id) return value;
            }
            return null;
        }
    }
}
